var path = require('path');

module.exports = function(grunt) {

	grunt.initConfig({

		// Import package manifest
		pkg: grunt.file.readJSON("package.json"),

		// Concat
		concat: {
			adminjs: {
				src: ["src/banners/js.ban", "src/js/ix_server_family_admin_script.js"],
				dest: "plugin/js/ix_server_family_admin_script.js"
			},
			js: {
				src: ["node_modules/slick-carousel/slick/slick.min.js", "node_modules/lightbox2/dist/js/lightbox.min.js", "node_modules/tablesorter/dist/js/jquery.tablesorter.min.js", "node_modules/tablesorter/dist/js/parsers/parser-metric.min.js", "node_modules/fixto/dist/fixto.min.js", "src/banners/js.ban", "src/js/ix_server_family_script.js"],
				dest: "plugin/js/ix_server_family_script.js"
			},
			css: {
				src: ["node_modules/lightbox2/dist/css/lightbox.css", "node_modules/slick-carousel/slick/slick.css", "node_modules/slick-carousel/slick/slick-theme.css", "src/banners/css.ban", "src/css/ix_server_family_style.css"],
				dest: "plugin/css/ix_server_family_style.css"
			},
			php: {
				src: ["src/banners/php.ban", "src/php/ix-server-family.php"],
				dest: "plugin/ix-server-family.php"
			},
			phpfo: {
				src: ["src/banners/php.ban", "src/php/filterOptions.php"],
				dest: "plugin/php/filterOptions.php"
			},
			phpsf: {
				src: ["src/banners/php.ban", "src/php/server_family.php"],
				dest: "plugin/php/server_family.php"
			},
			phpsff: {
				src: ["src/banners/php.ban", "src/php/server_family_filter.php"],
				dest: "plugin/php/server_family_filter.php"
			}
		},

		// Lint
		jshint: {
			files: ["src/js/ix_server_family_admin_script.js", "src/js/ix_server_family_script.js"],
			options: {
				jshintrc: ".jshintrc"
			}
		},

		// Copy
		copy: {
			img: {
				expand: true,
				cwd: 'src/img/',
				src: ['*', '!*.db'],
				dest: 'plugin/img/',
			},

			fonts: {
				expand: true,
				cwd: 'node_modules/slick-carousel/slick/fonts/',
				src: ['*', '!*.db'],
				dest: 'plugin/fonts/',
			},

			lightbox: {
				expand: true,
				cwd: 'node_modules/lightbox2/dist/images/',
				src: ['*', '!*.db'],
				dest: 'plugin/img/',
			},

			slick: {
				src: 'node_modules/slick-carousel/slick/ajax-loader.gif',
				dest: 'plugin/img/ajax-loader.gif',
			}
		},


		// Uglify
		uglify: {
			jsadmin: {
				src: ["plugin/js/ix_server_family_admin_script.js"],
				dest: "plugin/js/ix_server_family_admin_script.min.js"
			},
			js: {
				src: ["plugin/js/ix_server_family_script.js"],
				dest: "plugin/js/ix_server_family_script.min.js"
			},
			options: {
				preserveComments: "all"
			}
		},

		// Watch for changes
		watch: {
			files: ["src/css/*", "src/js/*", "src/php/*", "src/img/*", "src/banners/*.meta"],
			tasks: ["default"]
		},

		// Update banners
		makeBanners: {
		    phpBanner: {
				src: "src/banners/php.meta",
				dest: "src/banners/php.ban"
		    },
		    jsBanner: {
				src: "src/banners/js.meta",
				dest: "src/banners/js.ban"
		    },
		    cssBanner: {
				src: "src/banners/css.meta",
				dest: "src/banners/css.ban"
		    },
		    name: "<%= pkg.nice_name %>",
		    description: "<%= pkg.description %>",
		    version: "<%= pkg.version %>",
		    bugs: "<%= pkg.bugs.url %>",
		    author: "<%= pkg.author %>",
		    homepage: "<%= pkg.homepage %>",
		    license: "<%= pkg.license %>",
		    license_url: "<%= pkg.license_url %>",
		    repository: "<%= pkg.repository.url %>"
		},

		replaceCSSpaths: {
			css: "plugin/css/ix_server_family_style.css"

		},

		// Zips the plugin folder to make a release
		compress: {
			main: {
				options: {
					archive: 'release/<%= pkg.name %>-<%= pkg.version %>.zip'
    			},
    			files: [
      				{expand: true, cwd: 'plugin/', src: ['**/*'], dest: '<%= pkg.name %>/'}, // includes files in path and its subdirs
		    	]
  			}
		},

		// Minify css
		cssmin: {
			options: {
				keepSpecialComments: 0,
				roundingPrecision: -1,
				shorthandCompacting: false
			},
			css: {
				src: ['plugin/css/ix_server_family_style.css'],
				dest: 'plugin/css/ix_server_family_style.min.css'
			},
		},

		// Renders Readme.md to a PDF file
		markdownpdf: {
    		options: {
    			paperBorder: "2.5cm",
    			cssPath: "doc/css/pdf-style.css"
          	},
    		files: {
      			src: "readme.md",
      			dest: "plugin/"
    		}
  		}
	});

	grunt.registerTask( "makeBanners", "Prepares banners.", function() {
		var now = new Date().toISOString();
		var config = grunt.config.get("makeBanners");
		var phpContent = grunt.file.read(config.phpBanner.src);
		var jsContent = grunt.file.read(config.jsBanner.src);
		var cssContent = grunt.file.read(config.cssBanner.src);
		grunt.file.write(config.phpBanner.dest, replace(phpContent, now));
		grunt.file.write(config.jsBanner.dest, replace(jsContent, now));
		grunt.file.write(config.cssBanner.dest, replace(cssContent, now));


		function replace(content, now) {
			content = content.replace("$$$author_email$$$", config.author.email);
			content = content.replace("$$$author_name$$$", config.author.name);
			content = content.replace("$$$author_url$$$", config.author.url);
			content = content.replace("$$$bugs$$$", config.bugs);
			content = content.replace("$$$description$$$", config.description);
			content = content.replace("$$$homepage$$$", config.homepage);
			content = content.replace("$$$license_name$$$", config.license);
			content = content.replace("$$$license_url$$$", config.license_url);
			content = content.replace("$$$name$$$", config.name);
			content = content.replace("$$$now$$$", now);
			content = content.replace("$$$repository$$$", config.repository);
			content = content.replace(new RegExp("\\$\\$\\$version\\$\\$\\$", "g"), config.version);

			return content;
		}

		console.log("makeBanners finished without errors.");

    });


	grunt.registerTask( "replaceCSSpaths", "Replaces URLs to images and fonts.", function() {
		var config = grunt.config.get("replaceCSSpaths");
		var css = grunt.file.read(config.css);
		grunt.file.write(config.css, replace(css));

		function replace(content, now) {

			content = content.replace("$$$repository$$$", config.repository);
			content = content.replace(new RegExp("../images/", "g"), "../img/");
			content = content.replace(new RegExp("./fonts/", "g"), "../fonts/");
			content = content.replace(new RegExp("'./", "g"), "'../img/");

			return content;
		}

		console.log("replaceCSSpaths finished without errors.");

    });

	grunt.loadNpmTasks("grunt-contrib-compress");
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks("grunt-contrib-cssmin");
	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks('grunt-markdown-pdf');

	grunt.registerTask("build", ["makeBanners", "concat", "replaceCSSpaths", "copy", "uglify", "cssmin" ]);
	//grunt.registerTask("release", ["default", "markdownpdf", "compress"]); // markdownpdf not worky worky
	grunt.registerTask("release", ["default", "compress"]);
	grunt.registerTask("default", ["jshint", "build"]);
};