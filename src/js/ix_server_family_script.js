var defaultGroup;

function safelyGetMetaValue( id, word ) {
	try {
	    return metas[ id ][ word ][ 0 ];
	} catch (e) {
	  	console.log(e);
	    return "";
	}
}

function getIDfromSlug( slug ) {
   return hashes[ slug ];
}

function bindFeatures(id, word) {
	jQuery( "#" + word ).html(safelyGetMetaValue( id, word ));
	if (safelyGetMetaValue( id, word ) === "") {
		jQuery( "#" + word ).parent("tr").addClass("ix-server-hide-row").removeClass("ix-server-show-row");
	} else {
		jQuery( "#" + word ).parent("tr").addClass("ix-server-show-row").removeClass("ix-server-hide-row");
	}
}

function changeServer(id, context, event) {
	if (event === "click") {
		jQuery( ".ix-server-table-row" ).removeClass( "ix-server-selected" );

		if (context) {
			jQuery( context ).addClass( "ix-server-selected" );
		} else {
			jQuery( ".ix-server-table-row[data-serverid='"+ id +"']" ).addClass( "ix-server-selected" );
		}
	}
	var words = [   "cpu",
					"memory",
					"chipset",
					"network",
					"networkOpt",
					"ipmi",
					"video",
					"formFactor",
					"dimensions",
					"fans",
					"mountingRails",
					"bays",
					"solidState",
					"opticalDrive",
					"controller",
					"raid",
					"expansionSlot",
					"usbPorts",
					"sata",
					"ide",
					"sports",
					"keyboard",
					"mouse",
					//"lan",
					"vga",
					"powerButton",
					"resetButton",
					"powerLED",
					"hddLED",
					"networkLED",
					"overheatLED",
					"failLED",
					"frontPorts",
					"powerSupply",
					"server_features",
					"backplane",
					"connectivity",
					"hostRaid",
					"sasCables"
				  	];

	jQuery( ".ix-server-tabs-title" ).html("iX-" + metas[ id ].title);
		for (var i = 0, len = words.length; i < len; i++) {
	  	bindFeatures(id, words[ i ]);
	}

	checkHeaderRows();

	jQuery( ".ix-server-family-slick" ).slick( "removeSlide", null, null, true );
	if ( safelyGetMetaValue( id, "front_image" ) !== "" ) {
			jQuery( ".ix-server-family-slick" ).slick( "slickAdd", "<div><div class='image'><img src='" + safelyGetMetaValue( id, "front_image" ) + "'/></div></div>" );
	}
	if ( safelyGetMetaValue( id, "rare_image" ) !== "" ) {
		jQuery( ".ix-server-family-slick" ).slick( "slickAdd", "<div><div class='image'><img src='" + safelyGetMetaValue( id, "rare_image" ) + "'/></div></div>" );
	}
	if ( safelyGetMetaValue( id, "third_image" ) !== "" ) {
		jQuery( ".ix-server-family-slick" ).slick( "slickAdd", "<div><div class='image'><img src='" + safelyGetMetaValue( id, "third_image" ) + "'/></div></div>" );
	}
	if ( safelyGetMetaValue( id, "fourth_image" ) !== "" ) {
		jQuery( ".ix-server-family-slick" ).slick( "slickAdd", "<div><div class='image'><img src='" + safelyGetMetaValue( id, "fourth_image" ) + "'/></div></div>" );
	}
	if ( safelyGetMetaValue( id, "fifth_image" ) !== "" ) {
		jQuery( ".ix-server-family-slick" ).slick( "slickAdd", "<div><div class='image'><img src='" + safelyGetMetaValue( id, "fifth_image" )+ "'/></div></div>" );
	}
}


function checkHeaderRows() {
	var headerRows = [	["ix-server-motherboard-row", "ix-server-mbr"],
					   	["ix-server-chassis-row", "ix-server-chr"],
						["ix-server-storage-row", "ix-server-str"],
						["ix-server-raid-row", "ix-server-rar"],
						["ix-server-expandability-row", "ix-server-exr"],
						["ix-server-io-row", "ix-server-ior"],
						["ix-server-front-panel-row", "ix-server-fpr"],
						["ix-server-power-row", "ix-server-pwr"],
						["ix-server-sas-row", "ix-server-sas"]
				     ];
	for (var i = 0, len = headerRows.length; i < len; i++) {

	  	if (jQuery("." + headerRows[ i ][ 1 ] + ".ix-server-show-row").length > 0) {
	  		jQuery("." + headerRows[ i ][ 0 ]).addClass("ix-server-show-row").removeClass("ix-server-hide-row");
	  	} else {
	  		jQuery("." + headerRows[ i ][ 0 ]).addClass("ix-server-hide-row").removeClass("ix-server-show-row");
	  	}
	}

}

//filter

function getChecked() {
	var hasDefault = false;
	var chcked = jQuery( ".ix-server-filter-select-list-checkbox:checked" ).map(function() {
    	var obj = { "group": jQuery( this ).data("group"), "value": jQuery( this ).val() };
    	if (defaultGroup === obj.group) {
    		hasDefault = true;
    	}
    	return obj;
	}).get();

    if (!defaultGroup && chcked.length > 0) {
    	defaultGroup = chcked[0].group;
    } else if ( chcked.length === 0 ) {
    	defaultGroup = null;
    } else if ( chcked.length === 1 ) {
    	defaultGroup = chcked[0].group;
    } else if (hasDefault === false) {
    	defaultGroup = chcked[0].group;
    }

	return chcked;
}

function countOptions(selector, filter) {
	var available;
	if (!defaultGroup) {
		jQuery(".ix-server-filter-select-list-checkbox").each(function() {
			//console.log(jQuery(this).attr("name"));
			//console.log(jQuery(this).data("group"));
			available = jQuery(".ix-server-list-table-body-row[data-"+ jQuery(this).data("group") +"='"+ jQuery(this).val() +"']").size();

			if (available > 0) {
				jQuery(this).prop("disabled", false).next(".ix-server-filter-count").text(" ("+available+")").closest(".ix-server-filter-select-list-item").removeClass("ix-server-option-disabled");
			} else {
				jQuery(this).prop("disabled", true).next(".ix-server-filter-count").text("").closest(".ix-server-filter-select-list-item").addClass("ix-server-option-disabled");
			}
		});

	} else {
		jQuery(".ix-server-filter-select-list-checkbox").each(function() {
			//console.log(jQuery(this).attr("name"));
			//console.log(jQuery(this).data("group"));
			if (jQuery(this).data("group") === defaultGroup) {

				jQuery(this).next(".ix-server-filter-count").text("");
			} else {

				if (filter) {
					available = jQuery( selector.join( ", " ) ).filter( filter.join( "" )+"[data-"+ jQuery(this).data("group") +"='"+ jQuery(this).val() +"']" ).size();
				} else {
					available = jQuery( selector.join( ", " ) ).filter("[data-"+ jQuery(this).data("group") +"='"+ jQuery(this).val() +"']").size();

				}

				if (available > 0) {
					jQuery(this).prop("disabled", false).next(".ix-server-filter-count").text(" ("+available+")").closest(".ix-server-filter-select-list-item").removeClass("ix-server-option-disabled");
				} else {
					jQuery(this).prop("disabled", true).next(".ix-server-filter-count").text("").closest(".ix-server-filter-select-list-item").addClass("ix-server-option-disabled");
				}
			}
		});
	}

}

function countGroups() {

	jQuery( ".ix-server-filter-select-group" ).each(function() {
		var ulid = jQuery( this ).val();
		if (jQuery( this ).parent("label").siblings("ul#" + ulid).find("input").not(":disabled").size() > 0) {
			jQuery( this ).prop("disabled", false).parent(".ix-server-filter-select-group-label").removeClass("ix-server-option-disabled");
		} else {
			jQuery( this ).prop("disabled", true).parent(".ix-server-filter-select-group-label").addClass("ix-server-option-disabled");
		}
	});
}
//end of filter

jQuery(document).ready(function(){
  	jQuery( ".ix-server-family-slick" ).slick({
	  infinite: true,
	  speed: 500,
  	  autoplay: true,
  	  autoplaySpeed: 2000,
	  cssEase: "linear"
	});

  	checkHeaderRows();

	jQuery( ".ix-server-table-row" ).click(function() {
	  	var id = jQuery( this ).data( "serverid" );
	  	jQuery(".hs-pop-up").data("form_model", "Family:" + server_family_name + ", Server: " + metas[ id ].title);

	  	changeServer(id, this, "click");

		if (window.location.href.indexOf("?ix-server=") > -1){
			history.pushState({slug: metas[ id ].slug}, document.title, window.location.href.substring(0, 11 + window.location.href.indexOf("?ix-server=")) + metas[ id ].slug );
		} else {
			history.pushState({slug: metas[ id ].slug}, document.title, window.location.href + "?ix-server=" + metas[ id ].slug );
		}
	});

	jQuery( ".ix-server-table-row" ).mouseenter(function() {
	  	var id = jQuery( this ).data( "serverid" );

	  	changeServer(id, this, "hover");

	});

	jQuery( ".ix-server-table-row" ).mouseleave(function() {
	  	var id = jQuery( ".ix-server-selected" ).data( "serverid" );

	  	changeServer(id, this, "hover");

	});


	jQuery( ".ix-server-tab" ).click(function() {
		jQuery( ".ix-server-tab" ).removeClass( "ix-server-tab-active" );
		jQuery( this ).addClass( "ix-server-tab-active" );
		var activeTarget = jQuery( this ).data( "show" );

		jQuery( ".ix-server-tab-target" ).removeClass( "ix-server-show" );
		jQuery( "." + activeTarget ).addClass( "ix-server-show" );

	});

	if (window.location.href.indexOf("/ix-server-family/") > -1) {
		window.onpopstate = function(event) {
		  if (event.state) {
		  	if (event.state.slug) {
		  		changeServer(getIDfromSlug(event.state.slug));
		  	} else {
		  		location.reload();
		  	}
		  } else {
		  	location.reload();
		  }

		};

	}


// filter

	countOptions(null, null);
	countGroups();

	jQuery( ".ix-server-filter-select-list-checkbox" ).change(function() {
		if ( jQuery( this ).prop( "checked" ) ) {
			jQuery( this ).closest(".ix-server-filter-select-list-item").addClass("ix-server-option-selected");
		} else {
			jQuery( this ).closest(".ix-server-filter-select-list-item").removeClass("ix-server-option-selected");
		}

		var checked = getChecked();
		var showed = 0;

		if (checked.length > 0) {
			var selector = [];
			var filter = [];

			for (var i = 0; i < checked.length; i++) {
				if ( checked[i].group === defaultGroup ) {
					selector.push(".ix-server-list-table-body-row[data-"+ defaultGroup +"='"+ checked[i].value +"']");
	   			} else {
	   				filter.push("[data-"+ checked[i].group +"='"+ checked[i].value +"']");
	   			}
			}

			//console.log(selector.join(", "));
			//console.log(filter.join(""));



			jQuery( ".ix-server-list-table-body-row" ).hide();
			if (filter.length > 0) {
				showed = jQuery( selector.join( ", " ) ).filter( filter.join( "" ) ).show().size();
				countOptions(selector, filter);
			} else {
				showed = jQuery( selector.join( ", " ) ).show().size();
				countOptions(selector, null);
			}

  		} else {
  			showed = jQuery( ".ix-server-list-table-body-row" ).show().size();
  			countOptions(null, null);
  		}

  		jQuery( ".ix-server-result-filtered" ).text( showed );
  		countGroups();

	});

	jQuery( ".ix-server-filter-select-group" ).change(function() {
		var ulid = jQuery( this ).val();
		if (jQuery( this ).prop("checked")) {
			jQuery( this ).parent("label").siblings("ul#" + ulid).find("input").not(":disabled").prop("checked", true).trigger("change");
		} else {
			jQuery( this ).parent("label").siblings("ul#" + ulid).find("input").prop("checked", false).trigger("change");
		}
	});

// end of filter

	jQuery( ".ix-server-list-table-body-row" ).click(function() {
		window.location.href = "/ix-server-family/" + jQuery( this ).data("family") + "/?ix-server=" + jQuery( this ).data("model");
	});

	if (jQuery( ".ix-server-table-img-link" ).size() > 0) {
		console.log( jQuery( ".ix-server-table-img-link" ).size() );
		lightbox.option({ "resizeDuration": 400, "wrapAround": true, "fadeDuration": 300, "disableScrolling": true, "positionFromTop": 300 });
		lightbox.init();
		lightbox.init();
	}

	jQuery(".ix-server-table, .ix-server-list-table").tablesorter();

});
