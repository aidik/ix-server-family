<?php
define('ix-server-includer', TRUE);

// Include filter options
include( plugin_dir_path( __FILE__ ) . 'php/filterOptions.php');

//Administration part

//Add links to the installed pluing overview page
add_filter( 'plugin_row_meta', 'ix_server_family_plugin_row_links', 10 ,2 );

function ix_server_family_plugin_row_links( $links, $file ) {
	if ( $file == plugin_basename( __FILE__ ) ) {
		$new_links = array(
					'<a href="https://bitbucket.org/aidik/ix-server-family/issues/" target="_blank">Report Bugs</a>',
					'<a href="'. plugin_dir_url( __FILE__ ) .'readme.pdf" target="_blank">Read Me</a>'
				);

		$links = array_merge( $links, $new_links );
	}

	return $links;
}

//Register post type
add_action( 'init', 'register_ix_server_post_type' );
function register_ix_server_post_type() {
	$labels = array(
		'name'				=> __( 'iX Servers', 'ix-server-family' ),
		'singular_name'		=> __( 'iX Server', 'ix-server-family' ),
		'menu_name'			=> __( 'iX Servers', 'ix-server-family' ),
		'name_admin_bar'	=> __( 'iX Server', 'ix-server-family' ),
		'add_new'			=> __( 'Add New', 'ix-server-family' ),
		'add_new_item'		=> __( 'Add New iX Server', 'ix-server-family' ),
		'new_item'			=> __( 'New iX Server', 'ix-server-family' ),
		'edit_item'			=> __( 'Edit iX Server', 'ix-server-family' ),
		'view_item'			=> __( 'View iX Server', 'ix-server-family' ),
		'all_items'			=> __( 'All iX Servers', 'ix-server-family' ),
		'search_items'		=> __( 'Search iX Servers', 'ix-server-family' ),
		'not_found'			=> __( 'Not even one iX Server found.', 'ix-server-family' ),
		'not_found_in_trash'=> __( 'Not even one iX Server found in Trash.', 'ix-server-family' )
	);

	$args = array(
		'labels'			=> $labels,
		'description'		=> __( 'iX Servers - Plugin for displaing and filtering Server Families and individual Servers.', 'ix-server-family' ),
		'public'			=> false,
		'publicly_queryable'=> false,
		'show_ui'			=> true,
		'show_in_menu'		=> true,
		'query_var'			=> true,
		'rewrite'			=> array( 'slug' => 'ix-server' ),
		'capability_type'	=> 'post', //array( 'iX Server', 'iX Servers' ),
		'has_archive'		=> false,
		'hierarchical'		=> true,
		'menu_position'		=> 15,
		'supports'			=> array( 'title' ),
		'menu_icon'			=> plugin_dir_url( __FILE__ ) . '/img/ix-server-family.png'
	);

	register_post_type( 'ix_server', $args );
}

// START OF TAXONOMY BLOCK

// Register Custom Taxonomy for Server Family
add_action( 'init', 'custom_sf_taxonomy', 0 );

function custom_sf_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Server Families', 'Taxonomy General Name', 'ix-server-family' ),
		'singular_name'              => _x( 'Server Family', 'Taxonomy Singular Name', 'ix-server-family' ),
		'menu_name'                  => __( 'Server Families', 'ix-server-family' ),
		'all_items'                  => __( 'All Items', 'ix-server-family' ),
		'parent_item'                => __( 'Parent Item', 'ix-server-family' ),
		'parent_item_colon'          => __( 'Parent Item:', 'ix-server-family' ),
		'new_item_name'              => __( 'New Item Name', 'ix-server-family' ),
		'add_new_item'               => __( 'Add New Item', 'ix-server-family' ),
		'edit_item'                  => __( 'Edit Item', 'ix-server-family' ),
		'update_item'                => __( 'Update Item', 'ix-server-family' ),
		'view_item'                  => __( 'View Item', 'ix-server-family' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'ix-server-family' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'ix-server-family' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'ix-server-family' ),
		'popular_items'              => __( 'Popular Items', 'ix-server-family' ),
		'search_items'               => __( 'Search Items', 'ix-server-family' ),
		'not_found'                  => __( 'Not Found', 'ix-server-family' ),
		'no_terms'                   => __( 'No items', 'ix-server-family' ),
		'items_list'                 => __( 'Items list', 'ix-server-family' ),
		'items_list_navigation'      => __( 'Items list navigation', 'ix-server-family' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite' 					 => array('slug' => 'ix-server-family', 'with_front' => false),
	);
	register_taxonomy( 'server_family', array( 'ix_server' ), $args );

}

// add Perex field to taxonomy overview
add_action( 'server_family_add_form_fields', 'add_perex_field', 10 );
function add_perex_field( $taxonomy ) {

	// Lets do something for security
	wp_nonce_field( 'ix_server_family_action', 'ix_server_family_nonce' );

    ?><div class="form-field">
        <label for="perex">Perex</label>
        <input type="text" name="perex" id="perex" value="">
		<p class="description">Bold part of description, usually one sentence.</p>
    </div><?php
}

// allow edit of Perex field in taxonomy item detail.
add_action( 'server_family_edit_form', 'edit_perex_field', 10, 2 );
function edit_perex_field( $tag, $taxonomy ) {

	// Lets do something for security
	wp_nonce_field( 'ix_server_family_action', 'ix_server_family_nonce' );

	//print_r( $tag );

    ?><table class="form-table">
		<tbody><tr class="form-field">
			<th scope="row"><label for="perex">Perex</label></th>
			<td><input name="perex" id="perex" type="text" value="<?php echo( get_term_meta( $tag->term_id, 'perex', true ) ) ?>" size="40" aria-required="true">
			<p class="description">Bold part of description, usually one sentence.</p></td>
		</tr></tbody>
	</table><?php
}

// add Family Type field to taxonomy overview
add_action( 'server_family_add_form_fields', 'add_family_type', 5 );
function add_family_type( $taxonomy ) {
	// Put filter options in the scope
	global 	$familyTypes;

	// Lets do something for security
	wp_nonce_field( 'ix_server_family_action', 'ix_server_family_nonce' );


    ?><div class="form-field">
        <label for="family_type">Server Family Type</label>
        <select name="family_type" id="family_type"><?php
        	echo '<option value="-1">' . _e('none', 'ix-server-family') . '</option>';
        	foreach ($familyTypes as $_ft_key => $_ft) {
                if ($_ft_key != "-1") {
                	echo '<option value="'.$_ft_key.'">'.$_ft.'</option>';
            	}
            }
        ?>
        </select>
		<p class="description">Slect Server Family Type.</p>
    </div><?php
}

// allow edit of Family Type field in taxonomy item detail.
add_action( 'server_family_edit_form', 'edit_family_type_field', 5, 2 );
function edit_family_type_field( $tag, $taxonomy ) {
	// Put filter options in the scope
	global 	$familyTypes;

	// Lets do something for security
	wp_nonce_field( 'ix_server_family_action', 'ix_server_family_nonce' );

	//print_r( $tag );

    ?><table class="form-table">
		<tbody><tr class="form-field">
			<th scope="row"><label for="family_type">Server Family Type</label></th>
	        <td><select name="family_type" id="family_type"><?php
        	echo '<option value="-1">' . _e('none', 'ix-server-family') . '</option>';
        	foreach ($familyTypes as $_ft_key => $_ft) {
                if ($_ft_key != "-1") {
                	echo '<option value="'.$_ft_key.'" '. selected( get_term_meta( $tag->term_id, 'family_type', true ), $_ft_key, false ) .'>'.$_ft.'</option>';
            	}
            }
        	?>
        		</select>
			<p class="description">Slect Server Family Type.</p></td>
		</tr></tbody>
	</table><?php
}


// add Hero Image field to taxonomy overview
add_action( 'server_family_add_form_fields', 'add_hero_field', 10 );
function add_hero_field( $taxonomy ) {

	// Lets do something for security
	wp_nonce_field( 'ix_server_family_action', 'ix_server_family_nonce' );

    ?><div class="form-field">
        <label for="hero">Hero</label>
        <input type="text" name="hero" id="hero_image" value="">
		<input type="button" id="hero-image-button" class="button" value="Select Hero Image" />
		<p class="description">Slect image to be used as Hero on this Family page.</p>
    </div><?php
}

// allow edit of Hero Image field in taxonomy item detail.
add_action( 'server_family_edit_form', 'edit_hero_field', 10, 2 );
function edit_hero_field( $tag, $taxonomy ) {

	// Lets do something for security
	wp_nonce_field( 'ix_server_family_action', 'ix_server_family_nonce' );

	//print_r( $tag );

    ?><table class="form-table">
		<tbody><tr class="form-field">
			<th scope="row"><label for="hero">Hero Image</label></th>
			<td><input name="hero" id="hero_image" type="text" value="<?php echo( get_term_meta( $tag->term_id, 'hero', true ) ) ?>" size="40" aria-required="true">
				<input type="button" id="hero-image-button" class="button" value="Select Hero Image" />
			<p class="description">Slect image to be used as Hero on this Family page.</p></td>
		</tr>
		<tr class="form-field">
			<th scope="row"><label for="id">ID</label></th>
			<td><strong><?php echo ( $tag->term_id ) ?></strong>
			<p class="description">Server Family ID.</p></td>
		</tr></tbody>
	</table><?php
}



// Save taxonomy meta fields.
add_action( 'created_server_family', 'save_sf_form_meta', 10, 2 );
function save_sf_form_meta( $term_id, $tt_id ){
	// Add nonce for security and authentication.
	$nonce_name   = $_POST['ix_server_family_nonce'];
	$nonce_action = 'ix_server_family_action';

	// Check if a nonce is set.
	if ( ! isset( $nonce_name ) )
		return;

	// Check if a nonce is valid.
	if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
		return;

	// Check if the user has permissions to save data.
	if ( ! current_user_can( 'edit_post', $term_id ) )
		return;


    if( isset( $_POST['perex'] ) && '' !== $_POST['perex'] ){
        $perex = sanitize_text_field( $_POST['perex'] );
        add_term_meta( $term_id, 'perex', $perex, true );
    }
    if( isset( $_POST['hero'] ) && '' !== $_POST['hero'] ){

		//Remove the base url of the site
		$url = get_site_url();
		$hero = str_replace( $url, '', $_POST['hero'] );

        $hero = sanitize_text_field( $hero );
        add_term_meta( $term_id, 'hero', $hero, true );
    }
    if( isset( $_POST['family_type'] ) && '' !== $_POST['family_type'] ){

        $family_type = sanitize_text_field( $_POST['family_type'] );
        add_term_meta( $term_id, 'family_type', $family_type, true );
    }

}


// Save edited taxonomy meta fields.
add_action( 'edited_server_family', 'edit_sf_form_meta', 10, 2 );
function edit_sf_form_meta( $term_id, $tt_id ){
	// Add nonce for security and authentication.
	$nonce_name   = $_POST['ix_server_family_nonce'];
	$nonce_action = 'ix_server_family_action';

	// Check if a nonce is set.
	if ( ! isset( $nonce_name ) )
		return;

	// Check if a nonce is valid.
	if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
		return;

	// Check if the user has permissions to save data.
	if ( ! current_user_can( 'edit_post', $term_id ) )
		return;


    if( isset( $_POST['perex'] ) && '' !== $_POST['perex'] ){
        $perex = sanitize_text_field( $_POST['perex'] );
        update_term_meta( $term_id, 'perex', $perex );
    }

    if( isset( $_POST['hero'] ) && '' !== $_POST['hero'] ){

		//Remove the base url of the site
		$url = get_site_url();
		$hero = str_replace( $url, '', $_POST['hero'] );

        $hero = sanitize_text_field( $hero );
        update_term_meta( $term_id, 'hero', $hero );
    }

    if( isset( $_POST['family_type'] ) && '' !== $_POST['family_type'] ){

        $family_type = sanitize_text_field( $_POST['family_type'] );
        update_term_meta( $term_id, 'family_type', $family_type, true );
    }

}

add_filter('manage_edit-server_family_columns', 'add_perex_and_id_column' );

// add column to the taxonomy overview
function add_perex_and_id_column( $columns ){
    //array_splice( $columns, 2, 0, __( 'Perex', 'ix-server-family' ) );
    $columns['perex'] = __( 'Perex', 'ix-server-family' );
    $columns['id'] = __( 'ID', 'ix-server-family' );
    return $columns;
}

add_filter('manage_server_family_custom_column', 'add_server_family_perex_column_content', 10, 3 );

// populate the column with perex data
function add_server_family_perex_column_content( $content, $column_name, $term_id ){

    if( $column_name !== 'perex' ){
        return $content;
    }

    $term_id = absint( $term_id );
    $perex = get_term_meta( $term_id, 'perex', true );

    if( !empty( $perex ) ){
        $content .= esc_attr( $perex );
    }

    return $content;
}

add_filter('manage_server_family_custom_column', 'add_server_family_id_column_content', 10, 3 );

// populate the column with perex data
function add_server_family_id_column_content( $content, $column_name, $term_id ){

    if( $column_name !== 'id' ){
        return $content;
    }

    $content .= esc_attr( absint( $term_id ) );

    return $content;
}



add_filter( 'manage_edit-server_family_sortable_columns', 'add_perex_and_id_column_sortable' );

// Column sortable - not really needed
function add_perex_and_id_column_sortable( $sortable ){
    $sortable[ 'perex' ] = 'perex';
    $sortable[ 'id' ] = 'id';
    return $sortable;
}

// END OF TAXONOMY BLOCK


add_action( 'add_meta_boxes', 'ix_server_family_add_html_boxes' );
function ix_server_family_add_html_boxes() {

		add_meta_box( 'ix_server_family_specs_box', __( 'Insert Server Specs', 'ix-server-family' ), 'render_ix_server_specs_box', 'ix_server' );
		add_meta_box( 'ix_server_family_img_box', __( 'Select Server Images', 'ix-server-family' ), 'render_ix_server_img_box', 'ix_server' );
		add_meta_box( 'ix_server_family_filter_box', __( 'Select Filter Options', 'ix-server-family' ), 'render_ix_server_filter_box', 'ix_server' );
// I'm not sure if we will need this
//		add_meta_box( 'ix_server_family_shorcode_box', __( 'Shortcode to insert into posts', 'ix-server-family' ), 'render_shortcode_box', 'ix_server' );


	}


function render_ix_server_specs_box( $post ) {

	// Lets do something for security
	wp_nonce_field( 'ix_server_family_action', 'ix_server_family_nonce' );

	// Get existing value from db
	$all_post_meta = get_post_meta( $post->ID );

	// Lets prepare variable to hold all the specs
	$specs = array();

	// I wonder if there is more elegant way how to populate this array.
	$specs['cpu'][0] = isset( $all_post_meta['cpu'][0] ) ? $all_post_meta['cpu'][0] : '';
	$specs['cpu'][1] = "CPU";

	$specs['chipset'][0] = isset( $all_post_meta['chipset'][0] ) ? $all_post_meta['chipset'][0] : '';
	$specs['chipset'][1] = "Chipset";

	$specs['memory'][0] = isset( $all_post_meta['memory'][0] ) ? $all_post_meta['memory'][0] : '';
	$specs['memory'][1] = "Memory";

	$specs['expansionSlot'][0] = isset( $all_post_meta['expansionSlot'][0] ) ? $all_post_meta['expansionSlot'][0] : '';
	$specs['expansionSlot'][1] = "Expansion Slot";

	$specs['sata'][0] = isset( $all_post_meta['sata'][0] ) ? $all_post_meta['sata'][0] : '';
	$specs['sata'][1] = "Onboard Storage Controller";

	$specs['controller'][0] = isset( $all_post_meta['controller'][0] ) ? $all_post_meta['controller'][0] : '';
	$specs['controller'][1] = "RAID Controller";

	$specs['network'][0] = isset( $all_post_meta['network'][0] ) ? $all_post_meta['network'][0] : '';
	$specs['network'][1] = "Networking";

	$specs['networkOpt'][0] = isset( $all_post_meta['networkOpt'][0] ) ? $all_post_meta['networkOpt'][0] : '';
	$specs['networkOpt'][1] = "Networking Options";

	$specs['video'][0] = isset( $all_post_meta['video'][0] ) ? $all_post_meta['video'][0] : '';
	$specs['video'][1] = "VGA/Audio";

	$specs['ipmi'][0] = isset( $all_post_meta['ipmi'][0] ) ? $all_post_meta['ipmi'][0] : '';
	$specs['ipmi'][1] = "Remote Management (IPMI)";

	$specs['lan'][0] = isset( $all_post_meta['lan'][0] ) ? $all_post_meta['lan'][0] : '';
	$specs['lan'][1] = "LAN";

	$specs['bays'][0] = isset( $all_post_meta['bays'][0] ) ? $all_post_meta['bays'][0] : '';
	$specs['bays'][1] = "Hard Drive Bays";

	$specs['opticalDrive'][0] = isset( $all_post_meta['opticalDrive'][0] ) ? $all_post_meta['opticalDrive'][0] : '';
	$specs['opticalDrive'][1] = "Peripheral Bays";

	$specs['powerSupply'][0] = isset( $all_post_meta['powerSupply'][0] ) ? $all_post_meta['powerSupply'][0] : '';
	$specs['powerSupply'][1] = "Power Supply";

	$specs['fans'][0] = isset( $all_post_meta['fans'][0] ) ? $all_post_meta['fans'][0] : '';
	$specs['fans'][1] = "Fans";

	$specs['formFactor'][0] = isset( $all_post_meta['formFactor'][0] ) ? $all_post_meta['formFactor'][0] : '';
	$specs['formFactor'][1] = "Form Factor";

	$specs['dimensions'][0] = isset( $all_post_meta['dimensions'][0] ) ? $all_post_meta['dimensions'][0] : '';
	$specs['dimensions'][1] = "Dimensions";

	$specs['mountingRails'][0] = isset( $all_post_meta['mountingRails'][0] ) ? $all_post_meta['mountingRails'][0] : '';
	$specs['mountingRails'][1] = "Mounting Rails";

	$specs['solidState'][0] = isset( $all_post_meta['solidState'][0] ) ? $all_post_meta['solidState'][0] : '';
	$specs['solidState'][1] = "Solid State Storage";

	$specs['raid'][0] = isset( $all_post_meta['raid'][0] ) ? $all_post_meta['raid'][0] : '';
	$specs['raid'][1] = "RAID Support";

	$specs['usbPorts'][0] = isset( $all_post_meta['usbPorts'][0] ) ? $all_post_meta['usbPorts'][0] : '';
	$specs['usbPorts'][1] = "USB Ports";

	$specs['ide'][0] = isset( $all_post_meta['ide'][0] ) ? $all_post_meta['ide'][0] : '';
	$specs['ide'][1] = "IDE";

	$specs['sports'][0] = isset( $all_post_meta['sports'][0] ) ? $all_post_meta['sports'][0] : '';
	$specs['sports'][1] = "Serial Ports";

	$specs['keyboard'][0] = isset( $all_post_meta['keyboard'][0] ) ? $all_post_meta['keyboard'][0] : '';
	$specs['keyboard'][1] = "PS/2 Keyboard";

	$specs['mouse'][0] = isset( $all_post_meta['mouse'][0] ) ? $all_post_meta['mouse'][0] : '';
	$specs['mouse'][1] = "PS/2 Mouse";

	$specs['vga'][0] = isset( $all_post_meta['vga'][0] ) ? $all_post_meta['vga'][0] : '';
	$specs['vga'][1] = "VGA";

	$specs['powerButton'][0] = isset( $all_post_meta['powerButton'][0] ) ? $all_post_meta['powerButton'][0] : '';
	$specs['powerButton'][1] = "Power Button";

	$specs['resetButton'][0] = isset( $all_post_meta['resetButton'][0] ) ? $all_post_meta['resetButton'][0] : '';
	$specs['resetButton'][1] = "System Reset Button";

	$specs['powerLED'][0] = isset( $all_post_meta['powerLED'][0] ) ? $all_post_meta['powerLED'][0] : '';
	$specs['powerLED'][1] = "Power LED";

	$specs['hddLED'][0] = isset( $all_post_meta['hddLED'][0] ) ? $all_post_meta['hddLED'][0] : '';
	$specs['hddLED'][1] = "Hard Drive Activity LED";

	$specs['networkLED'][0] = isset( $all_post_meta['networkLED'][0] ) ? $all_post_meta['networkLED'][0] : '';
	$specs['networkLED'][1] = "Network Activity LEDs";

	$specs['overheatLED'][0] = isset( $all_post_meta['overheatLED'][0] ) ? $all_post_meta['overheatLED'][0] : '';
	$specs['overheatLED'][1] = "System Overheat LED";

	$specs['failLED'][0] = isset( $all_post_meta['failLED'][0] ) ? $all_post_meta['failLED'][0] : '';
	$specs['failLED'][1] = "Power Fail LED";

	$specs['frontPorts'][0] = isset( $all_post_meta['frontPorts'][0] ) ? $all_post_meta['frontPorts'][0] : '';
	$specs['frontPorts'][1] = "Front Ports";

	$specs['backplane'][0] = isset( $all_post_meta['backplane'][0] ) ? $all_post_meta['backplane'][0] : '';
	$specs['backplane'][1] = "Backplane";

	$specs['connectivity'][0] = isset( $all_post_meta['connectivity'][0] ) ? $all_post_meta['connectivity'][0] : '';
	$specs['connectivity'][1] = "Connectivity";

	$specs['hostRaid'][0] = isset( $all_post_meta['hostRaid'][0] ) ? $all_post_meta['hostRaid'][0] : '';
	$specs['hostRaid'][1] = "Host Server RAID Controller or HBA Options";

	$specs['sasCables'][0] = isset( $all_post_meta['sasCables'][0] ) ? $all_post_meta['sasCables'][0] : '';
	$specs['sasCables'][1] = "SAS Cables";

	$serverFeatures = isset( $all_post_meta['server_features'][0] ) ? $all_post_meta['server_features'][0] : '';

	//html code
    echo '<table class="form-table">';
	echo '<tbody>';

	echo '<tr class="form-field">';
	echo '<th scope="row"><label for="serverFeatures">Features</label></th>';
	echo '<td>';
	wp_editor( $serverFeatures, 'server_features', array(
		'textarea_rows' => '20',
		'media_buttons' => false
		) );
	echo '</td>';
	echo '</tr>';

	foreach($specs as $key => $spec) {

	echo '<tr class="form-field">';
	echo '<th scope="row"><label for="'.$key.'">'.$spec[1].'</label></th>';
	echo '<td><input name="'.$key.'" id="'.$key.'" type="text" value="'.esc_attr( $spec[0] ).'" size="40" aria-required="false">';
	//	echo '<p class="description"></p>';
	echo '</td>';
	echo '</tr>';

	}

	echo '</tbody>';
	echo '</table>';
}




function render_ix_server_filter_box( $post ) {


	// Put filter options in the scope
	global 	$formFactorOptions,
			$cpuSocketOptions,
			$cpuTypeOptions,
			$maxMemoryOptions,
			$hddBayOptions,
			$hddPlugOptions,
			$hddTypeOptions,
			$networkOptions,
			$networkStandardOptions,
			$redundantPowerOptions;

	// Get existing value from db - again I wonder if the DB is really called twice
	$all_post_meta = get_post_meta( $post->ID );


	// Lets prepare variable to hold all the filter options
	$foptions = array();
	$fchecks = array();

	// I wonder if there is more elegant way how to populate this array.
	$foptions['formFactorOption'][0] = isset( $all_post_meta['formFactorOption'][0] ) ? $all_post_meta['formFactorOption'][0] : '';
	$foptions['formFactorOption'][1] = $formFactorOptions;
	$foptions['formFactorOption'][2] = 'Form Factor';

	$foptions['cpuSocketOption'][0] = isset( $all_post_meta['cpuSocketOption'][0] ) ? $all_post_meta['cpuSocketOption'][0] : '';
	$foptions['cpuSocketOption'][1] = $cpuSocketOptions;
	$foptions['cpuSocketOption'][2] = 'CPU Sockets';

	$foptions['cpuTypeOption'][0] = isset( $all_post_meta['cpuTypeOption'][0] ) ? $all_post_meta['cpuTypeOption'][0] : '';
	$foptions['cpuTypeOption'][1] = $cpuTypeOptions;
	$foptions['cpuTypeOption'][2] = 'CPU Type';

	$foptions['maxMemoryOption'][0] = isset( $all_post_meta['maxMemoryOption'][0] ) ? $all_post_meta['maxMemoryOption'][0] : '';
	$foptions['maxMemoryOption'][1] = $maxMemoryOptions;
	$foptions['maxMemoryOption'][2] = 'Max Memory';

	$foptions['hddBayOption'][0] = isset( $all_post_meta['hddBayOption'][0] ) ? $all_post_meta['hddBayOption'][0] : '';
	$foptions['hddBayOption'][1] = $hddBayOptions;
	$foptions['hddBayOption'][2] = 'Hard Drive Bays';

	$foptions['hddPlugOption'][0] = isset( $all_post_meta['hddPlugOption'][0] ) ? $all_post_meta['hddPlugOption'][0] : '';
	$foptions['hddPlugOption'][1] = $hddPlugOptions;
	$foptions['hddPlugOption'][2] = 'Hard Drive Plug Type';

	$foptions['hddTypeOption'][0] = isset( $all_post_meta['hddTypeOption'][0] ) ? $all_post_meta['hddTypeOption'][0] : '';
	$foptions['hddTypeOption'][1] = $hddTypeOptions;
	$foptions['hddTypeOption'][2] = 'Hard Drive Interface Type';

	$foptions['networkOption'][0] = isset( $all_post_meta['networkOption'][0] ) ? $all_post_meta['networkOption'][0] : '';
	$foptions['networkOption'][1] = $networkOptions;
	$foptions['networkOption'][2] = 'Network';

	$foptions['networkStandardOption'][0] = isset( $all_post_meta['networkStandardOption'][0] ) ? $all_post_meta['networkStandardOption'][0] : '';
	$foptions['networkStandardOption'][1] = $networkStandardOptions;
	$foptions['networkStandardOption'][2] = 'Networking Standard';

	$foptions['redundantPowerOption'][0] = isset( $all_post_meta['redundantPowerOption'][0] ) ? $all_post_meta['redundantPowerOption'][0] : '';
	$foptions['redundantPowerOption'][1] = $redundantPowerOptions;
	$foptions['redundantPowerOption'][2] = 'Redundant Power';


	$fchecks['vmwareready'][0] = isset( $all_post_meta['vmwareready'][0] ) ? $all_post_meta['vmwareready'][0] : '';
	$fchecks['vmwareready'][1] = 'VMWare Ready';

	$fchecks['bsdcert'][0] = isset( $all_post_meta['bsdcert'][0] ) ? $all_post_meta['bsdcert'][0] : '';
	$fchecks['bsdcert'][1] = 'BSD Certified';

	$fchecks['linuxcert'][0] = isset( $all_post_meta['linuxcert'][0] ) ? $all_post_meta['linuxcert'][0] : '';
	$fchecks['linuxcert'][1] = 'Linux Certified';

	$fchecks['gpuopti'][0] = isset( $all_post_meta['gpuopti'][0] ) ? $all_post_meta['gpuopti'][0] : '';
	$fchecks['gpuopti'][1] = 'GPU Optimized';

	$fchecks['frontio'][0] = isset( $all_post_meta['frontio'][0] ) ? $all_post_meta['frontio'][0] : '';
	$fchecks['frontio'][1] = 'Front I/O';

	$fchecks['shortdepth'][0] = isset( $all_post_meta['shortdepth'][0] ) ? $all_post_meta['shortdepth'][0] : '';
	$fchecks['shortdepth'][1] = 'Short Depth';

	//file_put_contents('/var/www/staging/wp-content/plugins/ix-server-family/debug/redundantPowerOptions.txt', print_r($redundantPowerOptions, true));

	// Html Code
    echo '<table class="form-table">';
	echo '<tbody>';
    foreach ($foptions as $_foption_key => $_foption) {
	echo '<tr class="form-field">';
	echo '<th scope="row"><label for="'.$_foption_key.'">'.$_foption[2].'</label></th>';
	echo '<td><select id="'.$_foption_key.'" name="'.$_foption_key.'">';
    echo '<option value="-1">' . _e('none', 'ix-server-family') . '</option>';

    	foreach ($_foption[1] as $_group_key => $_group) {
                if ($_group_key != "-1") {
                	echo '<option value="'.$_group_key.'" '. selected( $_foption[0], $_group_key, false ) .'>'.$_group.'</option>';
            	}
            }

    echo '</select>';
	echo '</td>';
	echo '</tr>';
	}

    foreach ($fchecks as $_fcheck_key => $_fcheck) {
	echo '<tr class="form-field">';
	echo '<th scope="row"><label for="'.$_fcheck_key.'">'.$_fcheck[1].'</label></th>';
	echo '<td><input type="checkbox" name="'.$_fcheck_key.'" value="1" '.checked( $_fcheck[0], 1, false ).' />';
	echo '</td>';
	echo '</tr>';
	}

	echo '</tbody>';
	echo '</table>';


}

function render_ix_server_img_box( $post ) {

		// Get existing value from db
		$front_image = get_post_meta( $post->ID, 'front_image', true );
		$rare_image = get_post_meta( $post->ID, 'rare_image', true );
		$third_image = get_post_meta( $post->ID, 'third_image', true );
		$fourth_image = get_post_meta( $post->ID, 'fourth_image', true );
		$fifth_image = get_post_meta( $post->ID, 'fifth_image', true );



		// If db is empty fill variables with empty strings
		if( empty( $front_image ) ) $front_image = '';
		if( empty( $rare_image ) ) $rare_image = '';
		if( empty( $third_image ) ) $third_image = '';
		if( empty( $fourth_image ) ) $fourth_image = '';
		if( empty( $fifth_image ) ) $fifth_image = '';


		// Html Code
		echo '<table class="form-table"><tbody><tr><th scope="row">';
		echo '<label for="front_image">' . __( 'Front Image', 'ix-server-family' ) . '</label></th>';
		echo '<td><input type="text" id="front_image" name="front_image" placeholder="' . esc_attr__( '', 'ix-server-family' ) . '" value="' . esc_attr__( $front_image ) . '">';
		echo '<input type="button" id="front-image-button" class="button" value="' . __( 'Select Image', 'ix-server-family' ) . '" /></td></tr>';

		echo '<tr><th scope="row">';
		echo '<label for="rare_image">' . __( 'Rear Image', 'ix-server-family' ) . '</label></th>';
		echo '<td><input type="text" id="rear_image" name="rare_image" placeholder="' . esc_attr__( '', 'ix-server-family' ) . '" value="' . esc_attr__( $rare_image ) . '">';
		echo '<input type="button" id="rear-image-button" class="button" value="' . __( 'Select Image', 'ix-server-family' ) . '" /></td></tr>';

		echo '<tr><th scope="row">';
		echo '<label for="third_image">' . __( 'third Image', 'ix-server-family' ) . '</label></th>';
		echo '<td><input type="text" id="third_image" name="third_image" placeholder="' . esc_attr__( '', 'ix-server-family' ) . '" value="' . esc_attr__( $third_image ) . '">';
		echo '<input type="button" id="third-image-button" class="button" value="' . __( 'Select Image', 'ix-server-family' ) . '" /></td></tr>';

		echo '<tr><th scope="row">';
		echo '<label for="fourth_image">' . __( 'fourth Image', 'ix-server-family' ) . '</label></th>';
		echo '<td><input type="text" id="fourth_image" name="fourth_image" placeholder="' . esc_attr__( '', 'ix-server-family' ) . '" value="' . esc_attr__( $fourth_image ) . '">';
		echo '<input type="button" id="fourth-image-button" class="button" value="' . __( 'Select Image', 'ix-server-family' ) . '" /></td></tr>';

		echo '<tr><th scope="row">';
		echo '<label for="fifth_image">' . __( 'fifth Image', 'ix-server-family' ) . '</label></th>';
		echo '<td><input type="text" id="fifth_image" name="fifth_image" placeholder="' . esc_attr__( '', 'ix-server-family' ) . '" value="' . esc_attr__( $fifth_image ) . '">';
		echo '<input type="button" id="fifth-image-button" class="button" value="' . __( 'Select Image', 'ix-server-family' ) . '" /></td></tr></tbody></table>';
		echo '<br />';




	}

add_action( 'save_post', 'ix_server_family_save_metabox' );

function ix_server_family_save_metabox( $post_id ) {

	//file_put_contents('/var/www/staging/wp-content/plugins/ix-server-family/debug/post.txt', print_r($post, true));
	//file_put_contents('/var/www/staging/wp-content/plugins/ix-server-family/debug/post_id.txt', print_r($post_id, true));

	// Add nonce for security and authentication.
	$nonce_name   = $_POST['ix_server_family_nonce'];
	$nonce_action = 'ix_server_family_action';

	// Check if a nonce is set.
	if ( ! isset( $nonce_name ) )
		return;

	// Check if a nonce is valid.
	if ( ! wp_verify_nonce( $nonce_name, $nonce_action ) )
		return;

	// Check if the user has permissions to save data.
	if ( ! current_user_can( 'edit_post', $post_id ) )
		return;

	$_POST = stripslashes_deep( $_POST );

	$post_meta = array();

	// Get user input
	$post_meta[ 'server_features' ] = isset( $_POST['server_features'] ) ? wp_kses_post( $_POST[ 'server_features' ] ) : '';
	$post_meta[ 'cpu' ] = isset( $_POST[ 'cpu' ] ) ? sanitize_text_field( $_POST[ 'cpu' ] ) : '';
	$post_meta[ 'memory' ] = isset( $_POST[ 'memory' ] ) ? sanitize_text_field( $_POST[ 'memory' ] ) : '';
	$post_meta[ 'chipset' ] = isset( $_POST[ 'chipset' ] ) ? sanitize_text_field( $_POST[ 'chipset' ] ) : '';
	$post_meta[ 'network' ] = isset( $_POST[ 'network' ] ) ? sanitize_text_field( $_POST[ 'network' ] ) : '';
	$post_meta[ 'networkOpt' ] = isset( $_POST[ 'networkOpt' ] ) ? sanitize_text_field( $_POST[ 'networkOpt' ] ) : '';
	$post_meta[ 'ipmi' ] = isset( $_POST[ 'ipmi' ] ) ? sanitize_text_field( $_POST[ 'ipmi' ] ) : '';
	$post_meta[ 'video' ] = isset( $_POST[ 'video' ] ) ? sanitize_text_field( $_POST[ 'video' ] ) : '';
	$post_meta[ 'formFactor' ] = isset( $_POST[ 'formFactor' ] ) ? sanitize_text_field( $_POST[ 'formFactor' ] ) : '';
	$post_meta[ 'dimensions' ] = isset( $_POST[ 'dimensions' ] ) ? sanitize_text_field( $_POST[ 'dimensions' ] ) : '';
	$post_meta[ 'fans' ] = isset( $_POST[ 'fans' ] ) ? sanitize_text_field( $_POST[ 'fans' ] ) : '';
	$post_meta[ 'mountingRails' ] = isset( $_POST[ 'mountingRails' ] ) ? sanitize_text_field( $_POST[ 'mountingRails' ] ) : '';
	$post_meta[ 'bays' ] = isset( $_POST[ 'bays' ] ) ? sanitize_text_field( $_POST[ 'bays' ] ) : '';
	$post_meta[ 'solidState' ] = isset( $_POST[ 'solidState' ] ) ? sanitize_text_field( $_POST[ 'solidState' ] ) : '';
	$post_meta[ 'opticalDrive' ] = isset( $_POST[ 'opticalDrive' ] ) ? sanitize_text_field( $_POST[ 'opticalDrive' ] ) : '';
	$post_meta[ 'controller' ] = isset( $_POST[ 'controller' ] ) ? sanitize_text_field( $_POST[ 'controller' ] ) : '';
	$post_meta[ 'raid' ] = isset( $_POST[ 'raid' ] ) ? sanitize_text_field( $_POST[ 'raid' ] ) : '';
	$post_meta[ 'expansionSlot' ] = isset( $_POST[ 'expansionSlot' ] ) ? sanitize_text_field( $_POST[ 'expansionSlot' ] ) : '';
	$post_meta[ 'usbPorts' ] = isset( $_POST[ 'usbPorts' ] ) ? sanitize_text_field( $_POST[ 'usbPorts' ] ) : '';
	$post_meta[ 'sata' ] = isset( $_POST[ 'sata' ] ) ? sanitize_text_field( $_POST[ 'sata' ] ) : '';
	$post_meta[ 'ide' ] = isset( $_POST[ 'ide' ] ) ? sanitize_text_field( $_POST[ 'ide' ] ) : '';
	$post_meta[ 'sports' ] = isset( $_POST[ 'sports' ] ) ? sanitize_text_field( $_POST[ 'sports' ] ) : '';
	$post_meta[ 'keyboard' ] = isset( $_POST[ 'keyboard' ] ) ? sanitize_text_field( $_POST[ 'keyboard' ] ) : '';
	$post_meta[ 'mouse' ] = isset( $_POST[ 'mouse' ] ) ? sanitize_text_field( $_POST[ 'mouse' ] ) : '';
	$post_meta[ 'lan' ] = isset( $_POST[ 'lan' ] ) ? sanitize_text_field( $_POST[ 'lan' ] ) : '';
	$post_meta[ 'vga' ] = isset( $_POST[ 'vga' ] ) ? sanitize_text_field( $_POST[ 'vga' ] ) : '';
	$post_meta[ 'powerButton' ] = isset( $_POST[ 'powerButton' ] ) ? sanitize_text_field( $_POST[ 'powerButton' ] ) : '';
	$post_meta[ 'resetButton' ] = isset( $_POST[ 'resetButton' ] ) ? sanitize_text_field( $_POST[ 'resetButton' ] ) : '';
	$post_meta[ 'powerLED' ] = isset( $_POST[ 'powerLED' ] ) ? sanitize_text_field( $_POST[ 'powerLED' ] ) : '';
	$post_meta[ 'hddLED' ] = isset( $_POST[ 'hddLED' ] ) ? sanitize_text_field( $_POST[ 'hddLED' ] ) : '';
	$post_meta[ 'networkLED' ] = isset( $_POST[ 'networkLED' ] ) ? sanitize_text_field( $_POST[ 'networkLED' ] ) : '';
	$post_meta[ 'overheatLED' ] = isset( $_POST[ 'overheatLED' ] ) ? sanitize_text_field( $_POST[ 'overheatLED' ] ) : '';
	$post_meta[ 'failLED' ] = isset( $_POST[ 'failLED' ] ) ? sanitize_text_field( $_POST[ 'failLED' ] ) : '';
	$post_meta[ 'frontPorts' ] = isset( $_POST[ 'frontPorts' ] ) ? sanitize_text_field( $_POST[ 'frontPorts' ] ) : '';
	$post_meta[ 'powerSupply' ] = isset( $_POST[ 'powerSupply' ] ) ? sanitize_text_field( $_POST[ 'powerSupply' ] ) : '';
	$post_meta[ 'backplane' ] = isset( $_POST[ 'backplane' ] ) ? sanitize_text_field( $_POST[ 'backplane' ] ) : '';
	$post_meta[ 'connectivity' ] = isset( $_POST[ 'connectivity' ] ) ? sanitize_text_field( $_POST[ 'connectivity' ] ) : '';
	$post_meta[ 'hostRaid' ] = isset( $_POST[ 'hostRaid' ] ) ? wp_kses( $_POST[ 'hostRaid' ], array( 'br' => array() ) ) : '';
	$post_meta[ 'sasCables' ] = isset( $_POST[ 'sasCables' ] ) ? wp_kses( $_POST[ 'sasCables' ], array( 'br' => array() ) ) : '';;

	$post_meta[ 'front_image' ] = isset( $_POST[ 'front_image' ] ) ? esc_url( $_POST[ 'front_image' ] ) : '';
	$post_meta[ 'rare_image' ] = isset( $_POST[ 'rare_image' ] ) ? esc_url( $_POST[ 'rare_image' ] ) : '';
	$post_meta[ 'third_image' ] = isset( $_POST[ 'third_image' ] ) ? esc_url( $_POST[ 'third_image' ] ) : '';
	$post_meta[ 'fourth_image' ] = isset( $_POST[ 'fourth_image' ] ) ? esc_url( $_POST[ 'fourth_image' ] ) : '';
	$post_meta[ 'fifth_image' ] = isset( $_POST[ 'fifth_image' ] ) ? esc_url( $_POST[ 'fifth_image' ] ) : '';

	$post_meta[ 'formFactorOption' ] = isset( $_POST[ 'formFactorOption' ] ) ? sanitize_text_field( $_POST[ 'formFactorOption' ] ) : '';
	$post_meta[ 'cpuSocketOption' ] = isset( $_POST[ 'cpuSocketOption' ] ) ? sanitize_text_field( $_POST[ 'cpuSocketOption' ] ) : '';
	$post_meta[ 'cpuTypeOption' ] = isset( $_POST[ 'cpuTypeOption' ] ) ? sanitize_text_field( $_POST[ 'cpuTypeOption' ] ) : '';
	$post_meta[ 'maxMemoryOption' ] = isset( $_POST[ 'maxMemoryOption' ] ) ? sanitize_text_field( $_POST[ 'maxMemoryOption' ] ) : '';
	$post_meta[ 'hddBayOption' ] = isset( $_POST[ 'hddBayOption' ] ) ? sanitize_text_field( $_POST[ 'hddBayOption' ] ) : '';
	$post_meta[ 'hddPlugOption' ] = isset( $_POST[ 'hddPlugOption' ] ) ? sanitize_text_field( $_POST[ 'hddPlugOption' ] ) : '';
	$post_meta[ 'hddTypeOption' ] = isset( $_POST[ 'hddTypeOption' ] ) ? sanitize_text_field( $_POST[ 'hddTypeOption' ] ) : '';
	$post_meta[ 'networkOption' ] = isset( $_POST[ 'networkOption' ] ) ? sanitize_text_field( $_POST[ 'networkOption' ] ) : '';
	$post_meta[ 'networkStandardOption' ] = isset( $_POST[ 'networkStandardOption' ] ) ? sanitize_text_field( $_POST[ 'networkStandardOption' ] ) : '';
	$post_meta[ 'redundantPowerOption' ] = isset( $_POST[ 'redundantPowerOption' ] ) ? sanitize_text_field( $_POST[ 'redundantPowerOption' ] ) : '';
	$post_meta[ 'vmwareready' ] = isset( $_POST[ 'vmwareready' ] ) ? intval( $_POST[ 'vmwareready' ] ) : '';
	$post_meta[ 'bsdcert' ] = isset( $_POST[ 'bsdcert' ] ) ? intval( $_POST[ 'bsdcert' ] ) : '';
	$post_meta[ 'linuxcert' ] = isset( $_POST[ 'linuxcert' ] ) ? intval( $_POST[ 'linuxcert' ] ) : '';
	$post_meta[ 'gpuopti' ] = isset( $_POST[ 'gpuopti' ] ) ? intval( $_POST[ 'gpuopti' ] ) : '';
	$post_meta[ 'frontio' ] = isset( $_POST[ 'frontio' ] ) ? intval( $_POST[ 'frontio' ] ) : '';
	$post_meta[ 'shortdepth' ] = isset( $_POST[ 'shortdepth' ] ) ? intval( $_POST[ 'shortdepth' ] ) : '';


	//Remove the base url of the site
	$url = get_site_url();
	$post_meta[ 'front_image' ] = str_replace( $url, '', $post_meta[ 'front_image' ] );
	$post_meta[ 'rare_image' ] = str_replace( $url, '', $post_meta[ 'rare_image' ] );
	$post_meta[ 'third_image' ] = str_replace( $url, '', $post_meta[ 'third_image' ] );
	$post_meta[ 'fourth_image' ] = str_replace( $url, '', $post_meta[ 'fourth_image' ] );
	$post_meta[ 'fifth_image' ] = str_replace( $url, '', $post_meta[ 'fifth_image' ] );

	// Save meta options to db
	foreach($post_meta as $key => $meta) {
		update_post_meta( $post_id, $key, $meta );
	}
}

add_action( 'admin_enqueue_scripts', 'load_ix_server_family_admin_script' );
function load_ix_server_family_admin_script () {
	global $typenow, $ixserverversion;
	if ( $typenow == 'ix_server' ) {
		wp_enqueue_media();

		wp_register_script( 'ix_server_family_admin_script', plugin_dir_url( __FILE__ ) . 'js/ix_server_family_admin_script.min.js', array( 'jquery' ), $ixserverversion);
		wp_enqueue_script( 'ix_server_family_admin_script' );
	}
}

// Add Shortcode
add_shortcode( 'ix-server-family-filter', 'ix_server_family_generate' );
function ix_server_family_generate( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'ids' => '',
			'filterable' => 'false',
		),
		$atts,
		'ix-server-family-filter'
	);

	$atts['ids'] = str_replace(' ', '', $atts['ids']);
	$term_ids = explode(",", $atts['ids']);

	//file_put_contents('/var/www/staging/wp-content/plugins/ix-server-family/debug/term_ids.txt', print_r($term_ids, true));

    $args = array(
        'post_type' => 'ix_server',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'server_family',
                'field' => 'term_id', //can be set to ID
                'terms' => $term_ids //if field is ID you can reference by cat/term number
            )
        )
    );

	$wp_query = new WP_Query( $args );

	ob_start();
	include( plugin_dir_path( __FILE__ ) . 'php/server_family_filter.php');
    return ob_get_clean();

	//file_put_contents('/var/www/staging/wp-content/plugins/ix-server-family/debug/query.txt', print_r($query, true));

	//return '<h2>FUUUUU</h2>';

}

// Add js file to front end
add_action( 'wp_enqueue_scripts', 'load_ix_server_family_script' );
function load_ix_server_family_script() {
	global $ixserverversion;
    wp_register_script( 'ixsf_script',  plugin_dir_url( __FILE__ ) . 'js/ix_server_family_script.min.js', array( 'jquery' ), $ixserverversion);
    wp_enqueue_script( 'ixsf_script' );

}

add_action( 'wp_enqueue_scripts', 'load_ix_server_family_style', 100 );
function load_ix_server_family_style() {
	global $ixserverversion;
	wp_register_style( 'ixsf_style', plugin_dir_url( __FILE__ ) . 'css/ix_server_family_style.min.css', array(), $ixserverversion);
	wp_enqueue_style( 'ixsf_style' );

}

add_filter( 'taxonomy_template', 'server_family_taxonomy_template', 10 );
function server_family_taxonomy_template( $page_template ) {

    if ( is_tax('server_family') ) {
        $page_template = plugin_dir_path( __FILE__ ) . 'php/server_family.php';
    }
    return $page_template;
}

add_filter( 'pre_get_document_title', 'replace_archives_with_server_family', 50, 1);
function replace_archives_with_server_family($title)
{
    return preg_replace('/Archives/', 'Server Family', $title);
}

?>
