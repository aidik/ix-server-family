<?php
// Disable direct access
if( !defined( 'ix-server-includer' ) ) {

    die('Direct access not permitted');

}

$familyTypes = array(
    '-1'        =>      'N/A',
    'servers'   =>      'Servers (default)',
    'jbods'     =>      'JBODs'
);

$formFactorFork = array(
    'discrete' => array('1u', '2u', '3u','4u'),
    'multinode' => array('1u-dual', '2u-2', '2u-4', '3u-8', '3u-12', '4u-1', '4u-8')
);

$cpuTypeFork = array(
    'amd' => array('opteron6300', 'epyc', 'epyc7001'),
    'intel' => array('xeon1200', 'xeon2600', 'xeon2600v4v3', 'xeon4600', 'atom', 'corei3', 'celeron', 'pentium', 'xeon1200v6', 'xeon8800', 'xeonD1531', 'xeonLGA3647')
);

$formFactorOptions = array(
    '-1'        =>      'N/A',
    '1u'        =>      '1U',
    '2u'        =>      '2U',
    '3u'        =>      '3U',
    '4u'        =>      '4U',
    '7u'        =>      '7U',
    '1u-dual'   =>      '1U-Dual Node',
    '2u-2'      =>      '2U-2 Node',
    '2u-4'      =>      '2U-4 Node',
    '3u-8'      =>      '3U-8 Node',
    '3u-12'     =>      '3U-12 Node',
    '4u-1'      =>      '4U-4 Node',
    '4u-8'      =>      '4U-8 Node'
);

$cpuSocketOptions = array(
    '-1'        =>  'N/A',
    '1'         =>  '1',
    '1per-node' =>  '1 Per Node',
    '2'         =>  '2',
    '2per-node' =>  '2 Per Node',
    '4'         =>  '4',
    '8'         =>  '8'
);

$cpuTypeOptions = array(
    '-1'            =>  'N/A',
    'opteron6300'   =>  'AMD Opteron 6300 Series',
    'epyc'          =>  'Dual AMD EPYC™ 7000',
    'epyc7001'      =>  'Dual AMD EPYC™ 7001/7002',
    'epyc7002'      =>  'Single AMD EPYC™ 7002',
    'corei3'        =>  'Intel Core™i3',
    'celeron'       =>  'Intel Celeron',
    'pentium'       =>  'Intel Pentium',
    'xeon1200v6'    =>  'Intel Xeon E3-1200 v6/v5',
    'xeon1200'      =>  'Intel Xeon E3-1200 v3',
    'xeon2600'      =>  'Intel Xeon E5-2600',
    'xeon2600v4v3'  =>  'Intel Xeon E5-2600 v4/v3',
    'xeon4600'      =>  'Intel Xeon E5-4600',
    'xeon8800'      =>  'Intel Xeon E7-8800',
    'xeonD1531'     =>  'Intel Xeon D-1531',
    'xeonLGA3647'   =>  'Intel Xeon Scalable',
    'atom'          =>  'Intel Atom'
);

$maxMemoryOptions = array(
    '-1'        =>  'N/A',
    '32gb'      =>  '32GB',
    '64gb'      =>  '64GB',
    '128gb'     =>  '128GB',
    '256gb'     =>  '256GB',
    '512gb'     =>  '512GB',
    '768gb'     =>  '768GB',
    '1tb'       =>  '1TB',
    '15tb'      =>  '1.5TB',
    '2tb'       =>  '2TB',
    '3tb'       =>  '3TB',
    '4tb'       =>  '4TB',
    '6tb'       =>  '6TB',
    '8tb'       =>  '8TB',
    '10tb'      =>  '10TB',
    '12tb'      =>  '12TB',
    '14tb'      =>  '14TB',
    '15rtb'     =>  '15TB',
    '16tb'      =>  '16TB',
    '20tb'      =>  '20TB',
    '24tb'      =>  '24TB',
    '28tb'      =>  '28TB',
    '30tb'      =>  '30TB',
    '32tb'      =>  '32TB'
);

$hddBayOptions = array(
    '-1'        =>  'N/A',
    '1'         =>  '1',
    '2'         =>  '2',
    '3'         =>  '3',
    '4'         =>  '4',
    '6'         =>  '6',
    '8'         =>  '8',
    '10'        =>  '10',
    '12'        =>  '12',
    '16'        =>  '16',
    '24'        =>  '24',
    '32'        =>  '32',
    '36'        =>  '36',
    '44'        =>  '44',
    '48'        =>  '48',
    '56'        =>  '56',
    '60'        =>  '60',
    '64'        =>  '64',
    '72'        =>  '72',
    '90'        =>  '90',
    '102'        =>  '102',
);

$hddPlugOptions = array(
    '-1'            =>  'N/A',
    '2-3internal'   =>  '2.5"/3.5" Internal',
    '2-3hotswap'    =>  '2.5"/3.5" Hot-Swap',
    '2htoswap'      =>  '2.5" Hot-Swap'
);

$hddTypeOptions = array(
    '-1'            =>  'N/A',
    'satasas'   =>  'SATA/SAS',
    'nvme'    =>  'NVME'
);

$networkOptions = array(
    '-1'        =>  'N/A',
    'dualnic'   =>  'Dual NIC',
    '2pernode'  =>  '2 Per Node',
    'quadnic'   =>  'Quad NIC',
    '4pernode'  =>  '4 Per Node'
);

$networkStandardOptions = array(
    '-1'            =>  'N/A',
    '1gblan'        =>  '1Gbe Ports',
    '10gblan'       =>  '10Gbe Ports',
    '40gblan'       =>  '40Gbe Ports',
    'infiniband'    =>  'Infiniband'
);

$redundantPowerOptions = array(
    '-1'        =>  'N/A',
    'yes'       =>  '✓',
    'optional'  =>  'Optional',
    'no'        =>  '✗'
);

?>