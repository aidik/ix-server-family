<?php
$taxonomyID = $wp_query->get_queried_object_id();
$sfcat = get_term( $taxonomyID, 'server_family' );
if ( ! $sfcat || is_wp_error( $sfcat ) ) {
    $sfcat =  '';
  }
$name = $sfcat->name;
$perex = get_term_meta( $taxonomyID, 'perex', true );
$hero = get_term_meta( $taxonomyID, 'hero', true );
$family_type = get_term_meta( $taxonomyID, 'family_type', true );
$description = term_description( $taxonomyID );

    $args = array(
        'post_type' => 'ix_server',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'server_family',
                'field' => 'term_id', //can be set to ID
                'terms' => $taxonomyID //if field is ID you can reference by cat/term number
            )
        )
    );

    $wp_query = new WP_Query( $args );

$metas = array();
$hashes = array();
$amds = array();
$intels = array();

$selected;

while ( have_posts() ) {
    the_post();
    $postID = get_the_ID();
    $slug = $post->post_name;
    $metas[$postID] = get_post_meta( $postID );
    $metas[$postID]['title'] = get_the_title();
    $metas[$postID]['slug'] = $slug;
    $hashes[$slug] = $postID;
    if (strpos($cpuTypeOptions[$metas[$postID]['cpuTypeOption'][0]], 'AMD') !== false) {
        array_push($amds, $postID);
    } else {
        array_push($intels, $postID);
    }

}

$jmetas = json_encode($metas);
$jamds = json_encode($amds);
$jintels = json_encode($intels);
$jhashes = json_encode($hashes);
$jname = json_encode($name);

if ( isset( $_GET[ 'ix-server' ] ) && (isset( $hashes[ $_GET[ 'ix-server' ] ] ) ) ) {
    $selected = $hashes[ $_GET[ 'ix-server' ] ];
} elseif ( sizeof($intels) >0 ) {
    $selected = array_values($intels)[0];
} elseif ( sizeof($amds) > 0 ) {
    $selected = array_values($amds)[0];
}

//print_r($metas);
//print_r($wp_query);
get_header(); ?>
<script>
   var metas = <?php echo( $jmetas ) ?>;
   var amds = <?php echo( $jamds ) ?>;
   var intels = <?php echo( $jintels ) ?>;
   var hashes = <?php echo( $jhashes ) ?>;
   var server_family_name = <?php echo( $jname ) ?>;
</script>

<link href="https://static.ixsystems.co/uploads/pop_modal/css/lightbox.css" rel="stylesheet" />
<script src="https://static.ixsystems.co/uploads/pop_modal/js/lightbox.js"></script>
<script>
    jQuery(document).ready(function ($) { $(".modalpop a").simpleLightboxVideo(); });
</script>

    <div id="primary" class="ix-server-family-content-area content-area">
        <div class="ix-server-family-hero-container">
            <div class="ix-server-family-hero" style="background-image: url(<?php echo( $hero ); ?>);">
                <div class="ix-server-family-hero-dummy"></div>
                <div class="ix-server-family-hero-text">
                    <h1 class="ix-server-family-hero-h1"><?php echo( $perex ); ?></h1>
                    <div class="ix-server-family-hero-button modalpop"><a class="link-lightbox" href="#" data-videoid="jSW8vLj6Fmk" data-videosite="youtube"><img class="size-full wp-image-54633 aligncenter" src="https://www.ixsystems.com/wp-content/uploads/2016/11/Play_Button-1.png" alt="play_button" width="91" height="91"></a></div>
                    <div class="ix-server-family-hero-p"><?php echo( $description ); ?></div>
                </div>
            </div>
        </div>

        <div class="container">
        <main id="main" class="ix-server-family-site-main site-main" role="main">
        <?php if ( have_posts() ) : ?>

            <div class="ix-server-family-page-header page-header">
                <div class="ix-server-family-description-wrapper">
                <?php
                    //the_archive_title( '<h1 class="page-title">', '</h1>' );
                    echo '<h2 class="ix-server-family-perex">' . $perex . '</h2>';
                    the_archive_description( '<div class="ix-server-family-description">', '</div>' );
                ?>
                    <a style="width: 13em;margin: 1em 0 3em;letter-spacing: 0.5px;text-transform: uppercase;text-align: center;" class="et_pb_button et_pb_promo_button hs-pop-up hrefless">Get Quote</a>

                </div>
            </div><!-- .page-header -->
            <?php if ( $family_type == "jbods" ) : ?>
                <div class="ix-server-table-wrap">
                    <?php if (sizeof($intels) > 0) : ?>
                    <div class="ix-server-table-header-flexor ix-server-intel-table">
                        <h3 class="ix-server-table-header"><?php echo $name; ?></h3>
                        <span class="ix-server-table-notice">(Select a JBOD Enclosure, then click Get Quote)</span>
                    </div>
                    <table class="ix-server-table">
                        <thead class="ix-server-table-header">
                            <tr class="ix-server-table-header-row">
                                <th colspan="2" class="ix-server-table-title">Model</th>
                                <th class="ix-server-table-title">Size</th>
                                <th class="ix-server-table-title"># of Drives</th>
                                <th class="ix-server-table-title">Drive Type</th>
                                <th class="ix-server-table-title">Redundant Power</th>
                            </tr>
                        </thead>
                        <tbody>


                    <?php
                    // Start the Loop.
                    foreach($intels as $postID) {
                        echo '<tr data-serverid="'. $postID .'" class="ix-server-table-row'. (($selected == $postID ) ? ' ix-server-selected' : '') .'">';
                            echo '<td class="ix-server-table-cell">';
                                echo '<a class="ix-server-table-img-link" href="'. $metas[$postID]['front_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Front" ></a>';
                                echo '<a href="'. $metas[$postID]['rare_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Back"></a></td>';
                                echo '<a href="'. $metas[$postID]['third_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Additional Image"></a></td>';
                                echo '<a href="'. $metas[$postID]['fourth_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Additional Image"></a></td>';
                                echo '<a href="'. $metas[$postID]['fifth_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Additional Image"></a></td>';
                            echo '<td class="ix-server-table-cell ix-server-nowrap">iX-'. $metas[$postID]['title'] .'</td>';
                            echo '<td class="ix-server-table-cell">'. $formFactorOptions[$metas[$postID]['formFactorOption'][0]] .'</td>';
                            echo '<td class="ix-server-table-cell">'. $hddBayOptions[$metas[$postID]['hddBayOption'][0]] .'</td>';
                            echo '<td class="ix-server-table-cell">'. $hddTypeOptions[$metas[$postID]['hddTypeOption'][0]] .'</td>';
                            if ($redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] == "✓") {
                                echo '<td class="ix-server-table-cell ix-server-family-green">'. $redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] .'</td>';
                            } elseif ($redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] == "✗") {
                                echo '<td class="ix-server-table-cell ix-server-family-red">'. $redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] .'</td>';
                            } else {
                                echo '<td class="ix-server-table-cell">'. $redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] .'</td>';
                            }
                        echo '</tr>';
                    }

                    ?>

                    </tbody></table>
                    <?php endif; ?>
                </div>

            <?php else: ?>
                <div class="ix-server-table-wrap">
                    <?php if (sizeof($intels) > 0) : ?>
                        <div class="ix-server-table-header-flexor ix-server-intel-table">
                            <h3 class="ix-server-table-header">Intel-Based <?php echo $name; ?></h3>
                            <span class="ix-server-table-notice">(Select a Server, then click Get Quote)</span>
                        </div>
                        <table class="ix-server-table">
                            <thead class="ix-server-table-header">
                                <tr class="ix-server-table-header-row">
                                    <th colspan="2" class="ix-server-table-title">Model</th>
                                    <th class="ix-server-table-title">Size</th>
                                    <th class="ix-server-table-title">CPU Qty</th>
                                    <th class="ix-server-table-title">CPU</th>
                                    <th class="ix-server-table-title sorter-metric" data-metric-name-full="byte|Byte|BYTE" data-metric-name-abbr="b|B">Max Memory</th>
                                    <th class="ix-server-table-title"># of Drives</th>
                                    <th class="ix-server-table-title">Drive Type</th>
                                    <th class="ix-server-table-title">Network</th>
                                    <th class="ix-server-table-title">Redundant Power</th>
                                </tr>
                            </thead>
                            <tbody>


                        <?php
                        // Start the Loop.
                        foreach($intels as $postID) {
                            echo '<tr data-serverid="'. $postID .'" class="ix-server-table-row'. (($selected == $postID ) ? ' ix-server-selected' : '') .'">';
                                echo '<td class="ix-server-table-cell">';
                                    echo '<a class="ix-server-table-img-link" href="'. $metas[$postID]['front_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Front" ></a>';
                                    echo '<a href="'. $metas[$postID]['rare_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Back"></a></td>';
                                    echo '<a href="'. $metas[$postID]['third_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Additional Image"></a></td>';
                                    echo '<a href="'. $metas[$postID]['fourth_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Additional Image"></a></td>';
                                    echo '<a href="'. $metas[$postID]['fifth_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Additional Image"></a></td>';

                                echo '<td class="ix-server-table-cell ix-server-nowrap">iX-'. $metas[$postID]['title'] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $formFactorOptions[$metas[$postID]['formFactorOption'][0]] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $cpuSocketOptions[$metas[$postID]['cpuSocketOption'][0]] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $cpuTypeOptions[$metas[$postID]['cpuTypeOption'][0]] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $maxMemoryOptions[$metas[$postID]['maxMemoryOption'][0]] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $hddBayOptions[$metas[$postID]['hddBayOption'][0]] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $hddTypeOptions[$metas[$postID]['hddTypeOption'][0]] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $metas[$postID]['lan'][0] .'</td>';
                                if ($redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] == "✓") {
                                    echo '<td class="ix-server-table-cell ix-server-family-green">'. $redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] .'</td>';
                                } elseif ($redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] == "✗") {
                                    echo '<td class="ix-server-table-cell ix-server-family-red">'. $redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] .'</td>';
                                } else {
                                    echo '<td class="ix-server-table-cell">'. $redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] .'</td>';
                                }
                            echo '</tr>';
                        }

                        ?>

                        </tbody></table>
                    <?php endif; ?>

                    <?php if (sizeof($amds) > 0) : ?>
                        <div class="ix-server-table-header-flexor ix-server-amd-table">
                            <h3 class="ix-server-table-header">AMD-Based <?php echo $name; ?></h3>
                            <span class="ix-server-table-notice">(Select a Server, then click Get Quote)</span>
                        </div>
                        <table class="ix-server-table">
                            <thead class="ix-server-table-header">
                                <tr class="ix-server-table-header-row">
                                    <th colspan="2" class="ix-server-table-title">Model</th>
                                    <th class="ix-server-table-title">Size</th>
                                    <th class="ix-server-table-title">CPU Qty</th>
                                    <th class="ix-server-table-title">CPU</th>
                                    <th class="ix-server-table-title sorter-metric" data-metric-name-full="byte|Byte|BYTE" data-metric-name-abbr="b|B">Max Memory</th>
                                    <th class="ix-server-table-title"># of Drives</th>
                                    <th class="ix-server-table-title">Drive Type</th>
                                    <th class="ix-server-table-title">Network</th>
                                    <th class="ix-server-table-title">Redundant Power</th>
                                </tr>
                            </thead>
                            <tbody>


                        <?php
                        // Start the Loop.
                        foreach($amds as $postID) {
                            echo '<tr data-serverid="'. $postID .'" class="ix-server-table-row'. (($selected == $postID ) ? ' ix-server-selected' : '') .'">';
                                echo '<td class="ix-server-table-cell">';
                                    echo '<a class="ix-server-table-img-link" href="'. $metas[$postID]['front_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Front" ></a>';
                                    echo '<a href="'. $metas[$postID]['rare_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Back"></a></td>';
                                    echo '<a href="'. $metas[$postID]['third_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Additional Image"></a></td>';
                                    echo '<a href="'. $metas[$postID]['fourth_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Additional Image"></a></td>';
                                    echo '<a href="'. $metas[$postID]['fifth_image'][0] .'" data-lightbox="' . $metas[$postID]["slug"] . '" data-title="iX-' . $metas[$postID]["title"] . ' Additional Image"></a></td>';

                                echo '<td class="ix-server-table-cell ix-server-nowrap">iX-'. $metas[$postID]['title'] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $formFactorOptions[$metas[$postID]['formFactorOption'][0]] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $cpuSocketOptions[$metas[$postID]['cpuSocketOption'][0]] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $cpuTypeOptions[$metas[$postID]['cpuTypeOption'][0]] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $maxMemoryOptions[$metas[$postID]['maxMemoryOption'][0]] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $hddBayOptions[$metas[$postID]['hddBayOption'][0]] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $hddTypeOptions[$metas[$postID]['hddTypeOption'][0]] .'</td>';
                                echo '<td class="ix-server-table-cell">'. $metas[$postID]['lan'][0] .'</td>';
                                if ($redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] == "✓") {
                                    echo '<td class="ix-server-table-cell ix-server-family-green">'. $redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] .'</td>';
                                } elseif ($redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] == "✗") {
                                    echo '<td class="ix-server-table-cell ix-server-family-red">'. $redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] .'</td>';
                                } else {
                                    echo '<td class="ix-server-table-cell">'. $redundantPowerOptions[$metas[$postID]['redundantPowerOption'][0]] .'</td>';
                                }
                            echo '</tr>';
                        }

                        ?>

                        </tbody></table>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <div class="ix-server-tabs-wrap">
            <h3 class="ix-server-tabs-title">iX-<?php echo( $metas[$selected]['title'] ); ?></h3>
            <div class="ix-server-family-slick">
                <?php
                    if ($metas[$selected]['front_image'][0] != "") {
                        echo '<div><div class="image"><img src="'. $metas[$selected]['front_image'][0] .'"/></div></div>';
                    }
                    if ($metas[$selected]['rare_image'][0] != "") {
                        echo '<div><div class="image"><img src="'. $metas[$selected]['rare_image'][0] .'"/></div></div>';
                    }
                    if ($metas[$selected]['third_image'][0] != "") {
                        echo '<div><div class="image"><img src="'. $metas[$selected]['third_image'][0] .'"/></div></div>';
                    }
                    if ($metas[$selected]['fourth_image'][0] != "") {
                        echo '<div><div class="image"><img src="'. $metas[$selected]['fourth_image'][0] .'"/></div></div>';
                    }
                    if ($metas[$selected]['fifth_image'][0] != "") {
                        echo '<div><div class="image"><img src="'. $metas[$selected]['fifth_image'][0] .'"/></div></div>';
                    }
                ?>
            </div>
            <ul class="ix-server-tabs">
                <li class="ix-server-tab ix-server-tab-active" data-show="ix-server-specs">Specs</li>
                <li class="ix-server-tab" data-show="ix-server-description">Description</li>
                <li class="ix-server-foux-tab hs-pop-up">Get Quote</li>
            </ul>

            <div class="ix-server-tab-target-wrap">
            <div class="ix-server-tab-target ix-server-show ix-server-specs">
                <table class="ix-server-specs-table">
                    <tbody>
                        <tr class="ix-server-motherboard-row">
                            <th colspan="2">Motherboard</th>
                        </tr>
                        <tr class="ix-server-mbr <?php echo($metas[$selected]['cpu'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Processor Support</th>
                            <td id="cpu"><?php echo( $metas[$selected]['cpu'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-mbr <?php echo($metas[$selected]['memory'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>System Memory (Max.)*</th>
                            <td id="memory"><?php echo( $metas[$selected]['memory'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-mbr <?php echo($metas[$selected]['chipset'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Chipset</th>
                            <td id="chipset"><?php echo( $metas[$selected]['chipset'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-mbr <?php echo($metas[$selected]['network'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Networking</th>
                            <td id="network"><?php echo( $metas[$selected]['network'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-mbr <?php echo($metas[$selected]['networkOpt'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Networking Options</th>
                            <td id="networkOpt"><?php echo( $metas[$selected]['networkOpt'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-mbr <?php echo($metas[$selected]['ipmi'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Remote Management</th>
                            <td id="ipmi"><?php echo( $metas[$selected]['ipmi'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-mbr <?php echo($metas[$selected]['video'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>VGA/Audio</th>
                            <td id="video"><?php echo( $metas[$selected]['video'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-chassis-row">
                            <th colspan="2">Chassis</th>
                        </tr>
                        <tr class="ix-server-chr <?php echo($metas[$selected]['formFactor'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Form Factor</th>
                            <td id="formFactor"><?php echo( $metas[$selected]['formFactor'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-chr <?php echo($metas[$selected]['dimensions'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Dimensions</th>
                            <td id="dimensions"><?php echo( $metas[$selected]['dimensions'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-chr <?php echo($metas[$selected]['fans'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Cooling System</th>
                            <td id="fans"><?php echo( $metas[$selected]['fans'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-chr <?php echo($metas[$selected]['mountingRails'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Mounting Rails</th>
                            <td id="mountingRails"><?php echo( $metas[$selected]['mountingRails'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-storage-row">
                            <th colspan="2">Storage</th>
                        </tr>
                        <tr class="ix-server-str <?php echo($metas[$selected]['sata'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Onboard Storage Controller</th>
                            <td id="sata"><?php echo( $metas[$selected]['sata'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-str <?php echo($metas[$selected]['bays'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Hard Drive Bays</th>
                            <td id="bays"><?php echo( $metas[$selected]['bays'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-str <?php echo($metas[$selected]['solidState'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Solid State Storage</th>
                            <td id="solidState"><?php echo( $metas[$selected]['solidState'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-str <?php echo($metas[$selected]['opticalDrive'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Peripheral Bays</th>
                            <td id="opticalDrive"><?php echo( $metas[$selected]['opticalDrive'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-str <?php echo($metas[$selected]['backplane'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Backplane</th>
                            <td id="backplane"><?php echo( $metas[$selected]['backplane'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-str <?php echo($metas[$selected]['connectivity'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Connectivity</th>
                            <td id="connectivity"><?php echo( $metas[$selected]['connectivity'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-sas-row">
                            <th colspan="2">SAS Controller Options</th>
                        </tr>
                        <tr class="ix-server-sas <?php echo($metas[$selected]['hostRaid'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Host Server RAID Controller or HBA Options</th>
                            <td id="hostRaid"><?php echo( $metas[$selected]['hostRaid'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-sas <?php echo($metas[$selected]['sasCables'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>SAS Cables</th>
                            <td id="sasCables"><?php echo( $metas[$selected]['sasCables'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-raid-row">
                            <th colspan="2">Onboard RAID</th>
                        </tr>
                        <tr class="ix-server-rar <?php echo($metas[$selected]['controller'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>RAID Controller Options</th>
                            <td id="controller"><?php echo( $metas[$selected]['controller'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-rar <?php echo($metas[$selected]['raid'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>RAID Support</th>
                            <td id="raid"><?php echo( $metas[$selected]['raid'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-expandability-row">
                            <th colspan="2">Expandability</th>
                        </tr>
                        <tr class="ix-server-exr <?php echo($metas[$selected]['expansionSlot'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Expansion Slot</th>
                            <td id="expansionSlot"><?php echo( $metas[$selected]['expansionSlot'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-io-row">
                            <th colspan="2">Multiple I/O</th>
                        </tr>
                        <tr class="ix-server-ior <?php echo($metas[$selected]['usbPorts'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>USB Ports</th>
                            <td id="usbPorts"><?php echo( $metas[$selected]['usbPorts'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-ior <?php echo($metas[$selected]['ide'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>IDE</th>
                            <td id="ide"><?php echo( $metas[$selected]['ide'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-ior <?php echo($metas[$selected]['sports'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Serial Ports</th>
                            <td id="sports"><?php echo( $metas[$selected]['sports'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-ior <?php echo($metas[$selected]['keyboard'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>PS/2 Keyboard</th>
                            <td id="keyboard"><?php echo( $metas[$selected]['keyboard'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-ior <?php echo($metas[$selected]['mouse'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>PS/2 Mouse</th>
                            <td id="mouse"><?php echo( $metas[$selected]['mouse'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-ior <?php echo($metas[$selected]['vga'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>VGA</th>
                            <td id="vga"><?php echo( $metas[$selected]['vga'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-front-panel-row">
                            <th colspan="2">Front Panel</th>
                        </tr>
                        <tr class="ix-server-fpr <?php echo($metas[$selected]['powerButton'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Power Button</th>
                            <td id="powerButton"><?php echo( $metas[$selected]['powerButton'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-fpr <?php echo($metas[$selected]['resetButton'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>System Reset Button</th>
                            <td id="resetButton"><?php echo( $metas[$selected]['resetButton'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-fpr <?php echo($metas[$selected]['powerLED'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Power LED</th>
                            <td id="powerLED"><?php echo( $metas[$selected]['powerLED'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-fpr <?php echo($metas[$selected]['hddLED'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Hard Drive Activity LED</th>
                            <td id="hddLED"><?php echo( $metas[$selected]['hddLED'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-fpr <?php echo($metas[$selected]['networkLED'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Network Activity LEDs</th>
                            <td id="networkLED"><?php echo( $metas[$selected]['networkLED'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-fpr <?php echo($metas[$selected]['overheatLED'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>System Overheat LED</th>
                            <td id="overheatLED"><?php echo( $metas[$selected]['overheatLED'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-fpr <?php echo($metas[$selected]['failLED'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Power Fail LED</th>
                            <td id="failLED"><?php echo( $metas[$selected]['failLED'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-fpr <?php echo($metas[$selected]['frontPorts'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Front Ports</th>
                            <td id="frontPorts"><?php echo( $metas[$selected]['frontPorts'][0] ); ?></td>
                        </tr>
                        <tr class="ix-server-power-row">
                            <th colspan="2">Power</th>
                        </tr>
                        <tr class="ix-server-pwr <?php echo($metas[$selected]['powerSupply'][0] == '' ? 'ix-server-hide-row' : 'ix-server-show-row'); ?>">
                            <th>Power Supply</th>
                            <td id="powerSupply"><?php echo( $metas[$selected]['powerSupply'][0] ); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="ix-server-tab-target ix-server-description" id="server_features">
                <?php echo( $metas[$selected]['server_features'][0] ); ?>
            </div>
            </div>
            </div>






<div class="ixhs-pop-up-box">
    <div class="ixhs-server-family">
        <div class="ixhs-pop-up-close"></div>
        <!--[if lte IE 8]>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
        <![endif]-->
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
        <script>
            var serverModel = new RegExp("[?&]ix-server=([^&#]*)").exec(window.location.href)||[null, null];
            if (serverModel[ 1 ] !== null) {
                serverModel[ 1 ]=metas[ hashes [ serverModel[ 1 ] ] ].title;
            }
            ixhsOptions = {
                "includeUTM": true,
                "includeExtras": true,
                "extras": [
                            {"enabled": true, "name": "form_model", "value": "Family: <?php echo( $name ); ?>, Server: " + serverModel[1]},
                            {"enabled": true, "name": "form_domain", "value": "iXsystems.com"},
                            {"enabled": false, "name": "form_location", "value": "null"},
                            {"enabled": false, "name": "form_use_case", "value": "null"}
                           ],
                "popUp": true,
                "popUpTriggerClass": ".hs-pop-up",
              "hideAfterSubmit": ".ixhs-hide"
              };
              hbspt.forms.create({
                portalId: "6502416",
                formId: "96c596bb-1e9f-4035-9c5e-a474a315e778",
              onFormReady: function($form) {
                ixtegrateFormOnReady($form, ixhsOptions);
              },
              onFormSubmit: function($form) {
                ixtegrateFormOnSubmit($form, ixhsOptions);
                },
              onFormSubmitted: function($form) {
                ixtegrateFormOnSubmitted($form, ixhsOptions);
                }
            });
        </script>
    </div>
</div>














            <?php

        // If no content, include the "No posts found" template.
        else :
            get_template_part( 'template-parts/content', 'none' );

        endif;
        ?>

        </main><!-- .site-main -->
        </div>
    </div><!-- .content-area -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
