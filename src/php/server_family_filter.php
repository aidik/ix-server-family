<?php

//file_put_contents('/var/www/staging/wp-content/plugins/ix-server-family/debug/wp_query.txt', print_r($wp_query, true));

// Put filter options in the scope
global  $formFactorOptions,
        $cpuSocketOptions,
        $cpuTypeOptions,
        $maxMemoryOptions,
        $hddBayOptions,
        $hddPlugOptions,
        $networkOptions,
        $networkStandardOptions,
        $redundantPowerOptions,
        $formFactorFork,
        $cpuTypeFork;

//lets remove the  N/A option
// array_shift($formFactorOptions);
// array_shift($cpuSocketOptions);
// array_shift($cpuTypeOptions);
// array_shift($maxMemoryOptions);
// array_shift($hddBayOptions);
// array_shift($hddPlugOptions);
// array_shift($networkOptions);
// array_shift($networkStandardOptions);
// array_shift($redundantPowerOptions);        
unset( $formFactorOptions[ '-1' ]);
unset( $cpuSocketOptions[ '-1' ]);
unset( $cpuTypeOptions[ '-1' ]);
unset( $maxMemoryOptions[ '-1' ]);
unset( $hddBayOptions[ '-1' ]);
unset( $hddPlugOptions[ '-1' ]);
unset( $networkOptions[ '-1' ]);
unset( $networkStandardOptions[ '-1' ]);
unset( $redundantPowerOptions[ '-1' ]);


$metas = array();
$post_ids = wp_list_pluck( $wp_query->posts, 'ID', 'ID' );
$post_titles = wp_list_pluck( $wp_query->posts, 'post_title', 'ID' );
$post_slugs = wp_list_pluck( $wp_query->posts, 'post_name', 'ID' );

foreach ($post_ids as $key => $postID) {
    $metas[$postID] = get_post_meta( $postID );
    $metas[$postID]['title'] = $post_titles[$key];
    $metas[$postID]['slug'] = $post_slugs[$key];
    $metas[$postID]['terms'] = wp_get_post_terms($postID, "server_family");
}


?>


<script>
   var metas = <?php echo( json_encode($metas) ) ?>;
   //console.log( metas );
</script>

<table class="ix-server-filter-table">
    <thead class="ix-server-filter-table-head">
        <tr class="ix-server-filter-header-row">
            <th class="ix-server-filter-header-cell">Form Factor</th>
            <th class="ix-server-filter-header-cell">CPU Architecture</th>
            <th class="ix-server-filter-header-cell">CPU Sockets</th>
            <th class="ix-server-filter-header-cell">Max Memory</th>
            <th class="ix-server-filter-header-cell">Drives</th>
            <th class="ix-server-filter-header-cell">Drive Bays</th>
            <th class="ix-server-filter-header-cell">Network</th>
            <th class="ix-server-filter-header-cell">Options</th>
        </tr>
    </thead>
    <tbody class="ix-server-filter-table-body">
        <tr class="ix-server-filter-table-row">
            <td class="ix-server-filter-table-cell">
                <label class="ix-server-filter-select-group-label"><input class="ix-server-filter-select-group" type="checkbox" name="discrete" value="discrete">Discrete</label><br />
                <ul class="ix-server-filter-select-list" id="discrete">
                <?php
                    foreach ($formFactorFork['discrete'] as $formFactor) {
                        echo '<li class="ix-server-filter-select-list-item"><label><input data-group="formfactoroption" class="ix-server-filter-select-list-checkbox" type="checkbox" name="'.$formFactor.'" value="'.$formFactor.'">'. $formFactorOptions[ $formFactor ] .'<span class="ix-server-filter-count"></span></label></li>'; 
                    }
                ?>
                </ul>
                 <label class="ix-server-filter-select-group-label"><input class="ix-server-filter-select-group" type="checkbox" name="multinode" value="multinode">Multinode</label><br />
                <ul class="ix-server-filter-select-list" id="multinode">
                <?php
                    foreach ($formFactorFork['multinode'] as $formFactor) {
                        echo '<li class="ix-server-filter-select-list-item"><label><input data-group="formfactoroption" class="ix-server-filter-select-list-checkbox" type="checkbox" name="'.$formFactor.'" value="'.$formFactor.'">'. $formFactorOptions[ $formFactor ] .'<span class="ix-server-filter-count"></span></label></li>'; 
                    }
                ?>
                </ul>               
            </td>
            <td class="ix-server-filter-table-cell">
                 <label class="ix-server-filter-select-group-label"><input class="ix-server-filter-select-group" type="checkbox" name="amd" value="amd">AMD</label><br />
                <ul class="ix-server-filter-select-list" id="amd">
                <?php
                    foreach ($cpuTypeFork['amd'] as $cpuType) {
                        echo '<li class="ix-server-filter-select-list-item"><label><input data-group="cputypeoption" class="ix-server-filter-select-list-checkbox" type="checkbox" name="'.$cpuType.'" value="'.$cpuType.'">'. $cpuTypeOptions[ $cpuType ] .'<span class="ix-server-filter-count"></span></label></li>';    
                    }
                ?>
                </ul>
                 <label class="ix-server-filter-select-group-label"><input class="ix-server-filter-select-group" type="checkbox" name="intel" value="intel">Intel</label><br />
                <ul class="ix-server-filter-select-list" id="intel">
                <?php
                    foreach ($cpuTypeFork['intel'] as $cpuType) {
                        echo '<li class="ix-server-filter-select-list-item"><label><input data-group="cputypeoption" class="ix-server-filter-select-list-checkbox" type="checkbox" name="'.$cpuType.'" value="'.$cpuType.'">'. $cpuTypeOptions[ $cpuType ] .'<span class="ix-server-filter-count"></span></label></li>';    
                    }
                ?>
                </ul>               
            </td>
            <td class="ix-server-filter-table-cell">
                <ul class="ix-server-filter-select-list">
                <?php
                    foreach ($cpuSocketOptions as $cpuSocket => $cpuSocketName) {
                        echo '<li class="ix-server-filter-select-list-item"><label><input data-group="cpusocketoption" class="ix-server-filter-select-list-checkbox" type="checkbox" name="'.$cpuSocket.'" value="'.$cpuSocket.'">'. $cpuSocketName .'<span class="ix-server-filter-count"></span></label></li>';   
                    }
                ?>
                </ul> 
            </td>
            <td class="ix-server-filter-table-cell">
                <ul class="ix-server-filter-select-list">
                <?php
                    foreach ($maxMemoryOptions as $maxMemory => $maxMemoryName) {
                        echo '<li class="ix-server-filter-select-list-item"><label><input data-group="maxmemoryoption" class="ix-server-filter-select-list-checkbox" type="checkbox" name="'.$maxMemory.'" value="'.$maxMemory.'">'. $maxMemoryName .'<span class="ix-server-filter-count"></span></label></li>';   
                    }
                ?>
                </ul>               
            </td>
            <td class="ix-server-filter-table-cell">
                <ul class="ix-server-filter-select-list">
                <?php
                    foreach ($hddPlugOptions as $hddPlug => $hddPlugName) {
                        echo '<li class="ix-server-filter-select-list-item"><label><input data-group="hddplugoption" class="ix-server-filter-select-list-checkbox" type="checkbox" name="'.$hddPlug.'" value="'.$hddPlug.'">'. $hddPlugName .'<span class="ix-server-filter-count"></span></label></li>';   
                    }
                ?>
                </ul>               
            </td>
            <td class="ix-server-filter-table-cell">
                <ul class="ix-server-filter-select-list">
                <?php
                    foreach ($hddBayOptions as $hddBay => $hddBayName) {
                        echo '<li class="ix-server-filter-select-list-item"><label><input data-group="hddbayoption" class="ix-server-filter-select-list-checkbox" type="checkbox" name="'.$hddBay.'" value="'.$hddBay.'">'. $hddBayName .'<span class="ix-server-filter-count"></span></label></li>';   
                    }
                ?>
                </ul>               
            </td>
            <td class="ix-server-filter-table-cell">
                <ul class="ix-server-filter-select-list">
                <?php
                    foreach ($networkOptions as $network => $networkName) {
                        echo '<li class="ix-server-filter-select-list-item"><label><input data-group="networkoption" class="ix-server-filter-select-list-checkbox" type="checkbox" name="'.$network.'" value="'.$network.'">'. $networkName .'<span class="ix-server-filter-count"></span></label></li>';   
                    }
                ?>
                </ul>
                <div class="ix-server-filter-table-separator"></div> 
                <ul class="ix-server-filter-select-list">
                <?php
                    foreach ($networkStandardOptions as $networkStandard => $networkStandardName) {
                        echo '<li class="ix-server-filter-select-list-item"><label><input data-group="networkstandardoption" class="ix-server-filter-select-list-checkbox" type="checkbox" name="'.$networkStandard.'" value="'.$networkStandard.'">'. $networkStandardName .'<span class="ix-server-filter-count"></span></label></li>';   
                    }
                ?>
                </ul>                               
            </td>
            <td class="ix-server-filter-table-cell">
                <ul class="ix-server-filter-select-list">
                    <li class="ix-server-filter-select-list-item"><label><input data-group="vmwareready" class="ix-server-filter-select-list-checkbox" type="checkbox" name="vmwareready" value="1">VMWare Ready<span class="ix-server-filter-count"></span></label></li>
                    <li class="ix-server-filter-select-list-item"><label><input data-group="bsdcert" class="ix-server-filter-select-list-checkbox" type="checkbox" name="bsdcert" value="1">BSD Certified<span class="ix-server-filter-count"></span></label></li>
                    <li class="ix-server-filter-select-list-item"><label><input data-group="linuxcert" class="ix-server-filter-select-list-checkbox" type="checkbox" name="linuxcert" value="1">Linux Certified<span class="ix-server-filter-count"></span></label></li>
                    <li class="ix-server-filter-select-list-item"><label><input data-group="gpuopti" class="ix-server-filter-select-list-checkbox" type="checkbox" name="gpuopti" value="1">GPU Optimized<span class="ix-server-filter-count"></span></label></li>
                    <li class="ix-server-filter-select-list-item"><label><input data-group="frontio" class="ix-server-filter-select-list-checkbox" type="checkbox" name="frontio" value="1">Front I/O<span class="ix-server-filter-count"></span></label></li>
                    <li class="ix-server-filter-select-list-item"><label><input data-group="shortdepth" class="ix-server-filter-select-list-checkbox" type="checkbox" name="shortdepth" value="1">Short Depth<span class="ix-server-filter-count"></span></label></li>
                    <li class="ix-server-filter-select-list-item"><label><input data-group="redundantpoweroption" class="ix-server-filter-select-list-checkbox" type="checkbox" name="redundantpoweroption" value="1">Redudant Power<span class="ix-server-filter-count"></span></label></li>
                </ul>               
            </td>
        </tr>
    </tbody>
</table>

<h5 class="ix-server-result-count-heading">Filtered Results (<span class="ix-server-result-filtered"><?php echo ( sizeof( $metas ) ); ?></span> of <span class="ix-server-result-count"><?php echo ( sizeof( $metas ) ); ?></span>)</h5>
<table class="ix-server-list-table">
    <thead class="ix-server-list-table-head">
        <tr class="ix-server-list-table-head-row">
            <th class="ix-server-list-table-head-cell ix-server-list-table-model">Model</th>
            <th class="ix-server-list-table-head-cell ix-server-list-table-form-factor">Form Factor</th>
            <th class="ix-server-list-table-head-cell ix-server-list-table-cpu">CPU</th>
            <th class="ix-server-list-table-head-cell ix-server-list-table-network">Network</th>
            <th class="ix-server-list-table-head-cell ix-server-list-table-memory">Memory</th>
            <th class="ix-server-list-table-head-cell ix-server-list-table-bays">Drive Bays</th>
        </tr>
    </thead>
    <tbody  class="ix-server-list-table-body">
        <?php
          foreach ($metas as $server) {
            echo '<tr class="ix-server-list-table-body-row" data-family="'. $server['terms'][0]->slug .'" data-model="'. $server['slug'] .' " data-formfactoroption="'. $server['formFactorOption'][0] .'" data-cputypeoption="'. $server['cpuTypeOption'][0] .'" data-cpusocketoption="'. $server['cpuSocketOption'][0] .'" data-maxmemoryoption="'. $server['maxMemoryOption'][0] .'" data-hddplugoption="'. $server['hddPlugOption'][0] .'" data-hddbayoption="'. $server['hddBayOption'][0] .'" data-networkoption="'. $server['networkOption'][0] .'" data-networkstandardoption="'. $server['networkStandardOption'][0] .'" data-redundantpoweroption="'. (($server['redundantPowerOption'][0] == 'yes' || $server['redundantPowerOption'][0] == 'optional') ? '1' : '-1') .'" data-vmwareready="'. $server['vmwareready'][0] .'" data-bsdcert="'. $server['bsdcert'][0] .'" data-linuxcert="'. $server['linuxcert'][0] .'" data-gpuopti="'. $server['gpuopti'][0] .'" data-frontio="'. $server['frontio'][0] .'" data-shortdepth="'. $server['shortdepth'][0] .'">';
                echo '<td class="ix-server-list-table-cell">';
                    echo '<h4 class="ix-server-list-model-heading"><a class="ix-server-list-model-link" href="/ix-server-family/'. $server['terms'][0]->slug .'/?ix-server='. $server['slug'] .'">iX-'. $server['title'] .'</a></h4>';
                    echo '<img src="'. $server['front_image'][0] .'" class="ix-server-list-model-img" />';
                echo '</td>';
                echo '<td class="ix-server-list-table-cell">'. $server['formFactor'][0] .'</td>';
                echo '<td class="ix-server-list-table-cell">'. $server['cpu'][0] .'</td>';
                echo '<td class="ix-server-list-table-cell">'. $server['network'][0] .'</td>';
                echo '<td class="ix-server-list-table-cell">'. $server['memory'][0] .'</td>';
                echo '<td class="ix-server-list-table-cell">'. $server['bays'][0] .'</td>';
            echo '</tr>';
          } 
        ?>
    </tbody>
</table>    



